package com.abhitech.leave.service;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.repository.ClientRepo;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientSevice {
    
    @Autowired
    ClientRepo clientRepo;

    public GeneralResponse getAllClient() {
        return GeneralResponseUtil.resultSuccessWithData(clientRepo.findAll());
    }
}
