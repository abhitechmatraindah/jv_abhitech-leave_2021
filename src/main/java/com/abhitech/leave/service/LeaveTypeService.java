package com.abhitech.leave.service;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.model.LeaveType;
import com.abhitech.leave.model.User;
import com.abhitech.leave.repository.LeaveTypeRepo;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
public class LeaveTypeService {
    @Autowired
    LeaveTypeRepo leaveTypeRepo;

    @Autowired
    ActivityService activityService;

    @Autowired
    UserService userService;

    public GeneralResponse createLeaveType(HttpServletRequest httpServletRequest, LeaveType leaveType) {
        User user = userService.getUserFromSession(httpServletRequest);
        LeaveType result = leaveTypeRepo.save(leaveType);
        if (user != null && result != null) {
            activityService.createSimpleActivity(user.getUsername(), "" + leaveType.getLeaveCode(), "CREATE LEAVE TYPE", "LEAVECODE: " + leaveType.getLeaveCode(), "ADMIN");
            return GeneralResponseUtil.resultSuccessWithData(result);
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse updateLeaveType(LeaveType leaveType) {
        if (leaveTypeRepo.findById(leaveType.getId()).isPresent()) {
            LeaveType result = leaveTypeRepo.save(leaveType);
            if (result != null)
                return GeneralResponseUtil.resultSuccessWithData(result);
            return GeneralResponseUtil.resultFailureNoData();
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getAllLeaveType() {
        Iterable<LeaveType> result = leaveTypeRepo.findAll();
        if (result != null)
            return GeneralResponseUtil.resultSuccessWithData(result);
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getAllLeaveTypeEnabled() {
        Iterable<LeaveType> result = leaveTypeRepo.findByIsActive(true);
        if (result != null)
            return GeneralResponseUtil.resultSuccessWithData(result);
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getLeaveTypeById(long id) {
        Optional<LeaveType> result = leaveTypeRepo.findById(id);
        if (result.isPresent())
            return GeneralResponseUtil.resultSuccessWithData(result.get());
        return GeneralResponseUtil.resultFailureNoData();
    }
}
