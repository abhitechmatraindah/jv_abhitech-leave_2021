package com.abhitech.leave.service;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.LeaveBalance;
import com.abhitech.leave.model.LeaveType;
import com.abhitech.leave.model.User;
import com.abhitech.leave.repository.EmployeeRepo;
import com.abhitech.leave.repository.LeaveBalanceRepo;
import com.abhitech.leave.repository.LeaveTypeRepo;
import com.abhitech.leave.util.FileUtil;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class LeaveBalanceService {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    LeaveTypeRepo leaveTypeRepo;

    @Autowired
    LeaveBalanceRepo leaveBalanceRepo;

    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    RemovedLeaveBalanceService removedLeaveBalanceService;

    @Autowired
    ActivityService activityService;

    @Autowired
    UserService userService;

    public GeneralResponse updateLeaveBalanceFromFile(MultipartFile multipartFile) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
            File file = FileUtil.convertMultipartToFile(multipartFile);
            if (file.length() < Long.valueOf(1)) {
                throw new Exception("File is empty!");
            }
            XSSFWorkbook wb = new XSSFWorkbook(file);
            XSSFSheet sheet = wb.getSheetAt(0);
            Integer rows = sheet.getPhysicalNumberOfRows();
            List<LeaveBalance> leaveBalances = new ArrayList<>();
//            // SET ALL LEAVE BALANCE TO EXPIRED
//            Iterable<LeaveBalance> leaveBalanceList = leaveBalanceRepo.findByStatusAndExpired(true, false);
//            for (LeaveBalance leaveBalance : leaveBalanceList) {
//                leaveBalance.setStatus(false);
//                leaveBalance.setExpired(true);
//                leaveBalanceRepo.save(leaveBalance);
//                removedLeaveBalanceService.addRemovedLeaveBalance(leaveBalance);
//            }
            for (int rowCount = 1; rowCount < rows; rowCount++) {
                XSSFRow rw = sheet.getRow(rowCount);
                System.out.print("NO: " + rw.getCell(0) + ", ");
                logger.info("EmployeeId: " + rw.getCell(1));
                String employeeId = rw.getCell(1).getStringCellValue();
                Employee employee = employeeRepo.findByEmployeeIdAndIsActive(employeeId, true);
                if (employee != null) {
                    Iterable<LeaveBalance> existingLeaveBalance = leaveBalanceRepo.findByEmployee(employee);
                    for (LeaveBalance leaveBalance : existingLeaveBalance) {
                        leaveBalance.setStatus(false);
                        leaveBalance.setExpired(true);
                        leaveBalanceRepo.save(leaveBalance);
                        removedLeaveBalanceService.addRemovedLeaveBalance(leaveBalance);
                    }
                    LeaveBalance leaveBalance = new LeaveBalance();
                    leaveBalance.setEmployee(employee);
                    String leaveCode = rw.getCell(2).getStringCellValue();
                    LeaveType leaveType = leaveTypeRepo.findByLeaveCodeAndIsActive(leaveCode, true);
                    if (leaveType != null) {
                        leaveBalance.setLeaveType(leaveType);
                        leaveBalance.setBalance(Integer.parseInt(rw.getCell(3).getStringCellValue()));
                        leaveBalance.setExpiredDate(rw.getCell(4) != null && !rw.getCell(4).getStringCellValue().trim().isEmpty() ? simpleDateFormat.parse(rw.getCell(4).getStringCellValue()) : null);
                        leaveBalance.setNextAddDate(rw.getCell(5) != null && !rw.getCell(5).getStringCellValue().trim().isEmpty() ? simpleDateFormat.parse(rw.getCell(5).getStringCellValue()) : null);
                        leaveBalance.setNextRestartDate(rw.getCell(6) != null && !rw.getCell(6).getStringCellValue().trim().isEmpty() ? simpleDateFormat.parse(rw.getCell(6).getStringCellValue()) : null);
                        leaveBalances.add(leaveBalance);
                        logger.info("SUCCESS: " + employeeId);
                    } else {
                        logger.info("LEAVETYPE NOT FOUND WITH LEAVECODE: " + leaveCode);
                        throw new Exception("ERROR LEAVECODE ON ROW: " + rowCount);
                    }
                } else {
                    logger.info("EMPLOYEE NOT FOUND WITH EMPLOYEEID: " + employeeId);
                    throw new Exception("ERROR EMPLOYEEID ON ROW: " + rowCount);
                }
            }
            leaveBalanceRepo.saveAll(leaveBalances);
            file.delete();
            return GeneralResponseUtil.resultSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            return GeneralResponseUtil.resultFailureWithException(e);
        }
    }

    public GeneralResponse getListLeaveBalanceByEmployee(Employee employee) {
        List<LeaveBalance> result = leaveBalanceRepo.findByEmployeeAndStatusAndExpired(employee, true, false);
        System.out.println("EMPLOYEE EXIST: " + employee.getEmployeeName());
        if (result.size() > 0)
            return GeneralResponseUtil.resultSuccessWithData(result);
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getListLeaveBalanceByEmployeeUnsecured(String employeeId) {
        Optional<Employee> existingEmployeeOptional = employeeRepo.findByEmployeeIdEquals(employeeId);
        if (existingEmployeeOptional.isPresent()) {
            List<LeaveBalance> result = leaveBalanceRepo.findByEmployeeAndStatusAndExpired(existingEmployeeOptional.get(), true, false);
            if (result.size() > 0)
                return GeneralResponseUtil.resultSuccessWithData(result);
            return GeneralResponseUtil.resultFailureNoData();
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getEmployeeDetail(String employeeId) {
        Optional<Employee> existingEmployeeOptional = employeeRepo.findByEmployeeIdEquals(employeeId);
        if (existingEmployeeOptional.isPresent()) {
            return GeneralResponseUtil.resultSuccessWithData(existingEmployeeOptional.get());
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse deductBalance(HttpServletRequest httpServletRequest, LeaveBalance leaveBalanceToProcess, int amount) {
        User user = userService.getUserFromSession(httpServletRequest);
        Optional<LeaveBalance> leaveBalanceOptional = leaveBalanceRepo.findById(leaveBalanceToProcess.getId());
        if (user != null && leaveBalanceOptional.isPresent() && amount > 0) {
            LeaveBalance leaveBalance = leaveBalanceOptional.get();
            leaveBalance.setBalance(leaveBalance.getBalance() - amount);
            leaveBalance.setLastUpdatedDate(new Date());
            leaveBalanceRepo.save(leaveBalance);
            activityService.createSimpleActivity(user.getUsername(), "" + leaveBalance.getId(), "DEDUCT BALANCE", "AMOUNT: " + amount, "ADMIN");
            return GeneralResponseUtil.resultSuccessWithData(leaveBalance);
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse addBalance(HttpServletRequest httpServletRequest, LeaveBalance leaveBalanceToProcess, int amount) {
        User user = userService.getUserFromSession(httpServletRequest);
        Optional<LeaveBalance> leaveBalanceOptional = leaveBalanceRepo.findById(leaveBalanceToProcess.getId());
        if (leaveBalanceOptional.isPresent() && amount > 0) {
            LeaveBalance leaveBalance = leaveBalanceOptional.get();
            leaveBalance.setBalance(leaveBalance.getBalance() + amount);
            leaveBalance.setLastUpdatedDate(new Date());
            leaveBalanceRepo.save(leaveBalance);
            activityService.createSimpleActivity(user.getUsername(), "" + leaveBalance.getId(), "ADD BALANCE", "AMOUNT: " + amount, "ADMIN");
            return GeneralResponseUtil.resultSuccessWithData(leaveBalance);
        }
        return GeneralResponseUtil.resultFailureNoData();
    }
}
