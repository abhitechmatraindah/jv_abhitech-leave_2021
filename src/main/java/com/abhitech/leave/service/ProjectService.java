package com.abhitech.leave.service;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.repository.ProjectRepo;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {
    @Autowired
    ProjectRepo projectRepo;

    public GeneralResponse getAllProject() {
        return GeneralResponseUtil.resultSuccessWithData(projectRepo.findAll());
    }
    public GeneralResponse getAllGroupCompany() {
        return GeneralResponseUtil.resultSuccessWithData(projectRepo.findAllGroupCompanyDistinct());
    }
}
