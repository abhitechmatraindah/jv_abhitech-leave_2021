package com.abhitech.leave.service;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.model.LeaveBalance;
import com.abhitech.leave.model.RemovedLeaveBalance;
import com.abhitech.leave.repository.LeaveBalanceRepo;
import com.abhitech.leave.repository.RemovedLeaveBalanceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class RemovedLeaveBalanceService {

    @Autowired
    LeaveBalanceRepo leaveBalanceRepo;

    @Autowired
    RemovedLeaveBalanceRepo removedLeaveBalanceRepo;

    @Async
    public void addRemovedLeaveBalance(LeaveBalance leaveBalance) {
        RemovedLeaveBalance removedLeaveBalance = new RemovedLeaveBalance();
        removedLeaveBalance.setLeaveBalanceId(leaveBalance.getId());
        removedLeaveBalance.setGroupPayroll(leaveBalance.getGroupPayroll());
        removedLeaveBalance.setEmployee(leaveBalance.getEmployee());
        removedLeaveBalance.setBalance(leaveBalance.getBalance());
        removedLeaveBalance.setExpired(leaveBalance.isExpired());
        removedLeaveBalance.setExpiredDate(leaveBalance.getExpiredDate());
        removedLeaveBalance.setLastAddDate(leaveBalance.getLastAddDate());
        removedLeaveBalance.setLastRestartDate(leaveBalance.getLastRestartDate());
        removedLeaveBalance.setLastUpdatedDate(leaveBalance.getLastUpdatedDate());
        removedLeaveBalance.setLeaveType(leaveBalance.getLeaveType());
        removedLeaveBalance.setNextAddDate(leaveBalance.getNextAddDate());
        removedLeaveBalance.setNextRestartDate(leaveBalance.getNextRestartDate());
        removedLeaveBalanceRepo.save(removedLeaveBalance);
    }

    public GeneralResponse getAllRemovedLeaveBalanceByParams(String employeeId, String employeeName, String leaveName,
                                                             String expiredDate, String createdDate, String projectCode, String projectName,
                                                             String clientName, String clientCode, String groupCompany) {
        Iterable<RemovedLeaveBalance> result = removedLeaveBalanceRepo.findAllRemovedLeaveBalanceByParams(employeeId,employeeName,leaveName,expiredDate,createdDate,projectCode,projectName,clientName,clientCode,groupCompany);
        if (result != null)
            return com.abhitech.leave.util.GeneralResponseUtil.resultSuccessWithData(result);
        return com.abhitech.leave.util.GeneralResponseUtil.resultFailureNoData();
    }
}
