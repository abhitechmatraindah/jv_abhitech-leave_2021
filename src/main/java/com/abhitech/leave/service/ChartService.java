package com.abhitech.leave.service;

import com.abhitech.leave.bean.ChartBean;
import com.abhitech.leave.bean.ChartDatasetBean;
import com.abhitech.leave.model.LeaveType;
import com.abhitech.leave.repository.LeaveRequestRepo;
import com.abhitech.leave.repository.LeaveTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class ChartService {

    @Autowired
    LeaveRequestRepo leaveRequestRepo;

    @Autowired
    LeaveTypeRepo leaveTypeRepo;

    public ChartBean generateLeaveRequestChart10Days() throws Exception {
        LocalDate localDateFrom = LocalDate.now().minusDays(10);
        LocalDate localDateTo = LocalDate.now();

        ChartBean chartBean = new ChartBean();
        chartBean.setLabels(initializeLabels(localDateFrom, localDateTo));
        List<ChartDatasetBean> chartDatasetBeans = new ArrayList<>();

        Iterable<LeaveType> leaveTypes = leaveTypeRepo.findByIsActive(true);
        for (LeaveType leaveType : leaveTypes) {
            chartDatasetBeans.add(initializeDataSetBean(localDateFrom, localDateTo, leaveType));
        }
        chartBean.setDatasets(chartDatasetBeans);

        return chartBean;
    }

    public ChartBean generateLeaveRequestChart(String dateFrom, String dateTo, String employeeId, String departmentName, String projectCode) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate localDateFrom = LocalDate.parse(dateFrom, formatter);
        LocalDate localDateTo = LocalDate.parse(dateTo, formatter);

        ChartBean chartBean = new ChartBean();
        chartBean.setLabels(initializeLabels(localDateFrom, localDateTo));
        List<ChartDatasetBean> chartDatasetBeans = new ArrayList<>();

        Iterable<LeaveType> leaveTypes = leaveTypeRepo.findByIsActive(true);
        for (LeaveType leaveType : leaveTypes) {
            chartDatasetBeans.add(initializeDataSetBeanWithParam(localDateFrom, localDateTo, leaveType, employeeId, departmentName, projectCode));
        }
        chartBean.setDatasets(chartDatasetBeans);

        return chartBean;
    }

    private List<String> initializeLabels(LocalDate localDateFrom, LocalDate localDateTo) {
        List<String> labels = new ArrayList<>();
        while (localDateFrom.compareTo(localDateTo) != 0) {
            labels.add(localDateFrom.toString());
            localDateFrom = localDateFrom.plusDays(1);
        }
        labels.add(localDateTo.toString());
        return labels;
    }

    private ChartDatasetBean initializeDataSetBeanWithParam(LocalDate localDateFrom, LocalDate localDateTo, LeaveType leaveType, String employeeId, String departmentName, String projectCode) {
        ChartDatasetBean chartDatasetBean = new ChartDatasetBean();
        chartDatasetBean.setLabel(leaveType.getLeaveCode());
        List<Integer> datas = new ArrayList<>();
        while (localDateFrom.compareTo(localDateTo) != 0) {
            datas.add(leaveRequestRepo.sumLeaveAmountByParams(localDateFrom, leaveType.getId(), employeeId, departmentName, projectCode));
            localDateFrom = localDateFrom.plusDays(1);
        }
        datas.add(leaveRequestRepo.sumLeaveAmountByParams(localDateTo, leaveType.getId(), employeeId, departmentName, projectCode));
        chartDatasetBean.setData(datas);
        return chartDatasetBean;
    }

    private ChartDatasetBean initializeDataSetBean(LocalDate localDateFrom, LocalDate localDateTo, LeaveType leaveType) {
        ChartDatasetBean chartDatasetBean = new ChartDatasetBean();
        chartDatasetBean.setLabel(leaveType.getLeaveCode());
        List<Integer> datas = new ArrayList<>();
        while (localDateFrom.compareTo(localDateTo) != 0) {
            datas.add(leaveRequestRepo.sumLeaveAmountByDate(localDateFrom, leaveType.getId()));
            localDateFrom = localDateFrom.plusDays(1);
        }
        datas.add(leaveRequestRepo.sumLeaveAmountByDate(localDateTo, leaveType.getId()));
        chartDatasetBean.setData(datas);
        return chartDatasetBean;
    }

}
