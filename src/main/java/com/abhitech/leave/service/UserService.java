package com.abhitech.leave.service;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.model.User;
import com.abhitech.leave.repository.UserRepo;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepo userRepo;

    public GeneralResponse createUser(String username, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        return GeneralResponseUtil.resultSuccessWithData(userRepo.save(user));
    }

    public boolean validateUser(HttpServletRequest request, String username, String password) {
        User user = userRepo.findByUsernameAndPasswordEquals(username, password);
        if (user != null && user.getEnabled()) {
            request.getSession().setAttribute("USER", user);
            return true;
        }
        return false;
    }

    public User getUserFromSession(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("USER");
        Optional<User> dbUser = userRepo.findById(user != null ? user.getId() : 0);
        if (dbUser.isPresent()) {
            return dbUser.get();
        }
        return null;
    }

    public boolean isUserValid(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("USER");
        Optional<User> dbUser = userRepo.findById(user != null ? user.getId() : 0);
        return (dbUser.isPresent()) && (dbUser.get().getEnabled());
    }

    public void invalidateUser(HttpServletRequest request) {
        request.getSession().setAttribute("USER", null);
    }

    public GeneralResponse getAllUser() {
        return GeneralResponseUtil.resultSuccessWithData(userRepo.findAll());
    }

    public GeneralResponse disableUser(long id) {
        Optional<User> userOptional = userRepo.findById(id);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.setEnabled(false);
            userRepo.save(user);
            return GeneralResponseUtil.resultSuccessWithData(user);
        }
        return GeneralResponseUtil.resultFailureNoData();
    }
}
