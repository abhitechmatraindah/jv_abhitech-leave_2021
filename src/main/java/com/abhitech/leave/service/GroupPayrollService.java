package com.abhitech.leave.service;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.model.GroupPayroll;
import com.abhitech.leave.model.LeaveType;
import com.abhitech.leave.model.Project;
import com.abhitech.leave.model.User;
import com.abhitech.leave.repository.GroupPayrollRepo;
import com.abhitech.leave.repository.LeaveTypeRepo;
import com.abhitech.leave.repository.ProjectRepo;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GroupPayrollService {

    @Autowired
    GroupPayrollRepo groupPayrollRepo;

    @Autowired
    LeaveTypeRepo leaveTypeRepo;

    @Autowired
    ProjectRepo projectRepo;

    @Autowired
    ActivityService activityService;

    @Autowired
    UserService userService;

    public GeneralResponse getGroupPayrollById(Long id) {
        Optional<GroupPayroll> result = groupPayrollRepo.findById(id);
        if (result != null) {
            return GeneralResponseUtil.resultSuccessWithData(result.get());
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getGroupPayrollByProjectId(long projectId) {
        Optional<Project> projectOptional = projectRepo.findById(projectId);
        if (projectOptional.isPresent()) {
            List<GroupPayroll> groupPayrolls = groupPayrollRepo.findByProject(projectOptional.get());
            if (groupPayrolls.size() > 0) {
                return GeneralResponseUtil.resultSuccessWithData(groupPayrolls);
            }
            return GeneralResponseUtil.resultFailureNoData();
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse updateGroupPayroll(HttpServletRequest httpServletRequest, long id, long defaultLeaveTypeId) {
        User user = userService.getUserFromSession(httpServletRequest);
        Optional<GroupPayroll> groupPayrollOptional = groupPayrollRepo.findById(id);
        Optional<LeaveType> leaveTypeOptional = leaveTypeRepo.findById(defaultLeaveTypeId);
        if (user != null && groupPayrollOptional.isPresent() && leaveTypeOptional.isPresent()) {
            GroupPayroll groupPayrollFinal = groupPayrollOptional.get();
            groupPayrollFinal.setDefaultLeaveType(leaveTypeOptional.get());
            GroupPayroll result = groupPayrollRepo.save(groupPayrollFinal);
            activityService.createSimpleActivity(user.getUsername(), "" + groupPayrollFinal.getId(), "UPDATE GROUP PAYROLL DEFAULT LEAVE TYPE", "DEFAULT LEAVE TYPE ID: " + defaultLeaveTypeId, "ADMIN");
            if (result != null)
                return GeneralResponseUtil.resultSuccessWithData(result);
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse updateGroupPayrollLeaveType(long id, String[] leaveTypeIds) {
        Optional<GroupPayroll> groupPayrollOptional = groupPayrollRepo.findById(id);
        if (groupPayrollOptional.isPresent()) {
            List<LeaveType> leaveTypes = new ArrayList<>();
            for (String leaveTypeIdString : leaveTypeIds) {
                Long leaveTypeId = Long.parseLong(leaveTypeIdString);
                Optional<LeaveType> leaveTypeOptional = leaveTypeRepo.findById(leaveTypeId);
                if (leaveTypeOptional.isPresent()) {
                    leaveTypes.add(leaveTypeOptional.get());
                }
            }
            GroupPayroll groupPayrollFinal = groupPayrollOptional.get();
            groupPayrollFinal.setLeaveTypes(leaveTypes);
            GroupPayroll result = groupPayrollRepo.save(groupPayrollFinal);
            if (result != null) {
                return GeneralResponseUtil.resultSuccessWithData(result);
            }
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getGroupPayrollInactiveLeaveTypes(Long id) {
        Optional<GroupPayroll> result = groupPayrollRepo.findById(id);
        List<LeaveType> leaveTypeList = new ArrayList<>();
        if (result != null) {
            for (LeaveType leaveType : result.get().getLeaveTypes()) {
                if (!leaveType.isActive() || leaveType.isExpired() == true) {
                    leaveTypeList.add(leaveType);
                }
            }
            return GeneralResponseUtil.resultSuccessWithData(leaveTypeList);
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getGroupPayrollActiveLeaveTypes(Long id) {
        Optional<GroupPayroll> result = groupPayrollRepo.findById(id);
        List<LeaveType> leaveTypeList = new ArrayList<>();
        if (result != null) {
            for (LeaveType leaveType : result.get().getLeaveTypes()) {
                if (leaveType.isActive() && !leaveType.isExpired()) {
                    leaveTypeList.add(leaveType);
                }
            }
            return GeneralResponseUtil.resultSuccessWithData(leaveTypeList);
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getAllLeaveTypeEnabledNoDefaultLeaveType(Long id) {
        Optional<GroupPayroll> groupPayroll = groupPayrollRepo.findById(id);
        Iterable<LeaveType> leaveTypes = leaveTypeRepo.findByIsActive(true);
        List<LeaveType> leaveTypeList = new ArrayList<>();
        List<LeaveType> toBeDeleted = new ArrayList<>();

        if ((groupPayroll != null) && (leaveTypes != null)) {
            for (LeaveType leaveType : leaveTypes) {
                if (leaveType != groupPayroll.get().getDefaultLeaveType()) {
                    leaveTypeList.add(leaveType);
                }
                for (LeaveType groupLeaveType : groupPayroll.get().getLeaveTypes()) {
                    if (leaveType == groupLeaveType) {
                        toBeDeleted.add(groupLeaveType);
                    }
                }
            }
            leaveTypeList.removeAll(toBeDeleted);
            return GeneralResponseUtil.resultSuccessWithData(leaveTypeList);
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

}
