package com.abhitech.leave.service;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.bean.LeaveRequestCancelBean;
import com.abhitech.leave.model.*;
import com.abhitech.leave.repository.*;
import com.abhitech.leave.util.DateUtil;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class LeaveRequestService {
    @Autowired
    ActivityService activityService;

    @Autowired
    LeaveRequestRepo leaveRequestRepo;

    @Autowired
    LeaveBalanceRepo leaveBalanceRepo;

    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    LeaveTypeRepo leaveTypeRepo;

    @Autowired
    HolidayRepo holidayRepo;

    @Autowired
    UserService userService;

    public GeneralResponse createLeaveRequest(HttpServletRequest httpServletRequest, LeaveRequest leaveRequest) throws Exception {
        User user = userService.getUserFromSession(httpServletRequest);
        Date now = new Date();
        List<Holiday> holidays = holidayRepo.findByHolidayBetween(leaveRequest.getDateFrom(), leaveRequest.getDateTo());
        int holidayCounts = holidays.size();
        LeaveType leaveType = leaveRequest.getLeaveBalance().getLeaveType();
        if (leaveType.getDayType().equalsIgnoreCase("WORKINGDATE")) {
            int amountWorkingDays = DateUtil.getWorkingDaysBetweenTwoDates(leaveRequest.getDateFrom(), leaveRequest.getDateTo()) - holidayCounts;
            leaveRequest.setAmount(amountWorkingDays);
        } else if (leaveType.getDayType().equalsIgnoreCase("CALENDARDATE")) {
            int amountCalendarDays = DateUtil.getCalendarDaysBetweenTwoDates(leaveRequest.getDateFrom(), leaveRequest.getDateTo());
            leaveRequest.setAmount(amountCalendarDays);
        }
        if (user != null && leaveRequest.getLeaveBalance().getEmployee() != null) {
            Employee employee = leaveRequest.getLeaveBalance().getEmployee();
            if (employee.getContractEndDate() == null && employee.getContractEndDate().before(new Date())) {
                return GeneralResponseUtil.resultFailureWithException(new Exception("Employee contract is invalid"));
            }
            Optional<LeaveBalance> leaveBalanceOptional = leaveBalanceRepo.findById(leaveRequest.getLeaveBalance().getId());
            if (!leaveBalanceOptional.isPresent()) {
                throw new Exception("LeaveBalance not found with id: " + leaveRequest.getLeaveBalance().getId());
            }
            LeaveBalance leaveBalance = leaveBalanceOptional.get();
            if (leaveRequest.getDateTo().compareTo(leaveBalance.getExpiredDate()) > 0) {
                throw new Exception("Date requested exceeding the expired date of leaveBalance");
            }
            if (!leaveBalance.isExpired() && now.after(employee.getLeaveActivationDate())) {
                if (leaveBalance.getBalance() >= leaveRequest.getAmount() || leaveType.isNegativeAllowed()) {
                    if (leaveRequest.getAmount() < leaveType.getMaxDuration() || leaveType.getMaxDuration() == -1) {
                        // LEAVE REQUEST EXISTING WITH STATUS SUBMITTED CHECK IS DISABLED BY REQUEST
//                        LeaveRequest leaveRequestExisting = leaveRequestRepo.findByLeaveBalanceAndStatus(leaveBalance, "SUBMITTED");
//                        if (leaveRequestExisting == null) {
                        leaveRequest.setLeaveType(leaveType);
                        leaveRequest.setBalanceBefore(leaveBalance.getBalance());
                        leaveBalance.setBalance(leaveBalance.getBalance() - leaveRequest.getAmount());
                        leaveRequest.setBalanceAfter(leaveBalance.getBalance());
                        leaveRequest.setEmployee(employee);
                        leaveRequest.setStatus("SUBMITTED");
                        LeaveRequest result = leaveRequestRepo.save(leaveRequest);
                        leaveBalanceRepo.save(leaveBalance);
                        activityService.createSimpleActivity(user.getUsername(), "" + employee.getEmployeeId(), "CREATE LEAVE REQUEST", "DATEFROM: " + leaveRequest.getDateFrom() + ", DATETO: " + leaveRequest.getDateTo(), "ADMIN");
                        return GeneralResponseUtil.resultSuccessWithData(result);
//                        }
//                        return GeneralResponseUtil.resultFailureWithException(new Exception("Ongoing request still exist"));
                    }
                    return GeneralResponseUtil.resultFailureWithException(new Exception("Maximum Duration exceeded"));
                }
                return GeneralResponseUtil.resultFailureWithException(new Exception("Not enough balance"));
            } else {
                return GeneralResponseUtil.resultFailureWithException(new Exception("Balance is Expired or not Eligible"));
            }
        }
        return GeneralResponseUtil.resultFailureWithException(new Exception("Employee not found"));
    }

    public GeneralResponse createLeaveRequestUnsafe(LeaveRequest leaveRequest) throws Exception {
        Date now = new Date();
        List<Holiday> holidays = holidayRepo.findByHolidayBetween(leaveRequest.getDateFrom(), leaveRequest.getDateTo());
        int holidayCounts = holidays.size();
        LeaveType leaveType = leaveRequest.getLeaveBalance().getLeaveType();
        if (leaveType.getDayType().equalsIgnoreCase("WORKINGDATE")) {
            int amountWorkingDays = DateUtil.getWorkingDaysBetweenTwoDates(leaveRequest.getDateFrom(), leaveRequest.getDateTo()) - holidayCounts;
            leaveRequest.setAmount(amountWorkingDays);
        } else if (leaveType.getDayType().equalsIgnoreCase("CALENDARDATE")) {
            int amountCalendarDays = DateUtil.getCalendarDaysBetweenTwoDates(leaveRequest.getDateFrom(), leaveRequest.getDateTo());
            leaveRequest.setAmount(amountCalendarDays);
        }
        if (leaveRequest.getLeaveBalance().getEmployee() != null) {
            Employee employee = leaveRequest.getLeaveBalance().getEmployee();
            if (employee.getContractEndDate() == null && employee.getContractEndDate().before(new Date())) {
                return GeneralResponseUtil.resultFailureWithException(new Exception("Employee contract is invalid"));
            }
            Optional<LeaveBalance> leaveBalanceOptional = leaveBalanceRepo.findById(leaveRequest.getLeaveBalance().getId());
            if (!leaveBalanceOptional.isPresent()) {
                throw new Exception("LeaveBalance not found with id: " + leaveRequest.getLeaveBalance().getId());
            }
            LeaveBalance leaveBalance = leaveBalanceOptional.get();
            if (leaveRequest.getDateTo().compareTo(leaveBalance.getExpiredDate()) > 0) {
                throw new Exception("Date requested exceeding the expired date of leaveBalance");
            }
            if (!leaveBalance.isExpired() && now.after(employee.getLeaveActivationDate())) {
                if (leaveBalance.getBalance() >= leaveRequest.getAmount() || leaveType.isNegativeAllowed()) {
                    if (leaveRequest.getAmount() < leaveType.getMaxDuration() || leaveType.getMaxDuration() == -1) {
                        LeaveRequest leaveRequestExisting = leaveRequestRepo.findByLeaveBalanceAndStatus(leaveBalance, "SUBMITTED");
                        if (leaveRequestExisting == null) {
                            leaveRequest.setLeaveType(leaveType);
                            leaveRequest.setBalanceBefore(leaveBalance.getBalance());
                            leaveBalance.setBalance(leaveBalance.getBalance() - leaveRequest.getAmount());
                            leaveRequest.setBalanceAfter(leaveBalance.getBalance());
                            leaveRequest.setEmployee(employee);
                            leaveRequest.setStatus("SUBMITTED");
                            LeaveRequest result = leaveRequestRepo.save(leaveRequest);
                            leaveBalanceRepo.save(leaveBalance);
//                            activityService.createSimpleActivity(user.getUsername(), "" + employee.getEmployeeId(), "CREATE LEAVE REQUEST", "DATEFROM: " + leaveRequest.getDateFrom() + ", DATETO: " + leaveRequest.getDateTo(), "ADMIN");
                            return GeneralResponseUtil.resultSuccessWithData(result);
                        }
                        return GeneralResponseUtil.resultFailureWithException(new Exception("Ongoing request still exist"));
                    }
                    return GeneralResponseUtil.resultFailureWithException(new Exception("Maximum Duration exceeded"));
                }
                return GeneralResponseUtil.resultFailureWithException(new Exception("Not enough balance"));
            } else {
                return GeneralResponseUtil.resultFailureWithException(new Exception("Balance is Expired or not Eligible"));
            }
        }
        return GeneralResponseUtil.resultFailureWithException(new Exception("Employee not found"));
    }

    public GeneralResponse cancelLeaveRequest(LeaveRequestCancelBean cancelLeaveRequest) {
        Optional<LeaveRequest> leaveRequestOptionalExisting = leaveRequestRepo.findById(cancelLeaveRequest.getId());
        if (leaveRequestOptionalExisting.isPresent()) {
            LeaveRequest leaveRequest = leaveRequestOptionalExisting.get();
            leaveRequest.setStatus("CANCELLED");
            leaveRequestRepo.save(leaveRequest);
            LeaveBalance leaveBalance = leaveRequest.getLeaveBalance();
            int beforeBalance = leaveBalance.getBalance();
            if (cancelLeaveRequest.getReturnBalanceOption() == 1) {
                leaveBalance.setBalance(leaveBalance.getBalance() + leaveRequest.getAmount());
                leaveBalanceRepo.save(leaveBalance);
            } else if (cancelLeaveRequest.getReturnBalanceOption() == 2) {
                if (leaveRequest.getLeaveType().getDayType().equalsIgnoreCase("WORKINGDATE")) {
                    leaveBalance.setBalance(leaveBalance.getBalance() + (leaveRequest.getAmount() - DateUtil.getWorkingDaysBetweenTwoDates(leaveRequest.getDateFrom(), cancelLeaveRequest.getLeaveEndDate())));
                } else if (leaveRequest.getLeaveType().getDayType().equalsIgnoreCase("CALENDARDATE")) {
                    leaveBalance.setBalance(leaveBalance.getBalance() + (leaveRequest.getAmount() - DateUtil.getCalendarDaysBetweenTwoDates(new Date(leaveRequest.getDateFrom().getTime()), cancelLeaveRequest.getLeaveEndDate())));
                }
                leaveBalanceRepo.save(leaveBalance);
            }
            activityService.createSimpleActivity("ADMIN", leaveRequest.getEmployee().getEmployeeName() + "- ID: " + leaveRequest.getEmployee().getEmployeeId(), "Cancel leave request", "Balance before: " + beforeBalance + ", Balance after: " + leaveBalance.getBalance(), "ADMIN");
            return GeneralResponseUtil.resultSuccessWithData(leaveRequest);
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse updateLeaveRequest(LeaveRequest leaveRequest) {
        if (leaveRequestRepo.findById(leaveRequest.getId()).isPresent()) {
            LeaveRequest result = leaveRequestRepo.save(leaveRequest);
            if (result != null)
                return GeneralResponseUtil.resultSuccessWithData(result);
            return GeneralResponseUtil.resultFailureNoData();
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getAllLeaveRequest() {
        List<LeaveRequest> result = leaveRequestRepo.findAllByStatusOrderByCreatedDateDesc("SUBMITTED");
        if (result.size() > 0)
            return GeneralResponseUtil.resultSuccessWithData(result);
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getAllLeaveRequestByParams(String status, String employeeId, String employeeName, String leaveName,
                                                      String createdDate, String dateFrom, String dateTo, String projectCode, String projectName,
                                                      String clientName, String clientCode, String groupCompany) {
        if (status == "Submitted") {
            Iterable<LeaveRequest> result = leaveRequestRepo.findAllLeaveRequestByParamsAll(employeeId, employeeName, leaveName, createdDate, dateFrom, dateTo, projectCode, projectName, clientName, clientCode, groupCompany);
            if (result != null)
                return GeneralResponseUtil.resultSuccessWithData(result);
            return GeneralResponseUtil.resultFailureNoData();
        } else if (status == "Cancelled") {
            Iterable<LeaveRequest> result = leaveRequestRepo.findAllLeaveRequestByParamsCancelled(employeeId, employeeName, leaveName, createdDate, dateFrom, dateTo, projectCode, projectName, clientName, clientCode, groupCompany);
            if (result != null)
                return GeneralResponseUtil.resultSuccessWithData(result);
            return GeneralResponseUtil.resultFailureNoData();
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getLeaveRequestById(long id) {
        Optional<LeaveRequest> result = leaveRequestRepo.findById(id);
        if (result.isPresent())
            return GeneralResponseUtil.resultSuccessWithData(result.get());
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getLeaveRequestByEmployee(Employee employee) {
        List<LeaveRequest> result = leaveRequestRepo.findByEmployee(employee);
        if (result.size() > 0)
            return GeneralResponseUtil.resultSuccessWithData(result);
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getLeaveRequestByEmployeeId(String employeeId) {
        Employee employee = employeeRepo.getByEmployeeId(employeeId);
        if (employee == null)
            return GeneralResponseUtil.resultFailureNoData();
        List<LeaveRequest> result = leaveRequestRepo.findByEmployee(employee);
        result.forEach(leaveRequest -> System.out.println(leaveRequest.toString()));
        if (result.size() > 0)
            return GeneralResponseUtil.resultSuccessWithData(result);
        return GeneralResponseUtil.resultFailureNoData();
    }
}
