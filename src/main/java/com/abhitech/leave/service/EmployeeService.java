package com.abhitech.leave.service;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.LeaveType;
import com.abhitech.leave.repository.EmployeeRepo;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    LeaveBalanceService leaveBalanceService;

    @Autowired
    ActivityService activityService;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public GeneralResponse getEmployeeLeaveBalanceById(long id) {
        Optional<Employee> employeeOptional = employeeRepo.findById(id);
        if (employeeOptional.isPresent()) {
            return leaveBalanceService.getListLeaveBalanceByEmployee(employeeOptional.get());
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getAllEmployee() {
        Iterable<Employee> employees = employeeRepo.findAll();
        return GeneralResponseUtil.resultSuccessWithData(employees);
    }

    public GeneralResponse findAllEmployeeByParams(String employeeId, String employeeName,
                                                   String projectCode, String projectName, String clientName, String clientCode, String groupCompany) {
        Iterable<Employee> employees = employeeRepo.findAllEmployeeByParams(employeeId, employeeName, projectCode, projectName, clientName, clientCode, groupCompany);
        return GeneralResponseUtil.resultSuccessWithData(employees);
    }

    public GeneralResponse getEmployeeById(long id) {
        Optional<Employee> employeeOptional = employeeRepo.findById(id);
        if (employeeOptional.isPresent()) {
            return GeneralResponseUtil.resultSuccessWithData(employeeOptional.get());
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse setEmployeeLeaveActivationDateById(String employeeId, String enrolledDate, String leaveActivationDate, String contractDate, String contractEndDate, String probationEndDate) throws Exception {
        Employee employee = employeeRepo.getByEmployeeId(employeeId);
        if (employee != null) {
            String separator = ", ";
            employee.setDateEnrolled((enrolledDate != null && !enrolledDate.trim().isEmpty()) ? simpleDateFormat.parse(enrolledDate) : null);
            employee.setContractStartDate((contractDate != null && !contractDate.trim().isEmpty()) ? simpleDateFormat.parse(contractDate) : null);
            employee.setContractEndDate((contractEndDate != null && !contractEndDate.trim().isEmpty()) ? simpleDateFormat.parse(contractEndDate) : null);
            employee.setProbationEndDate((probationEndDate != null && !probationEndDate.trim().isEmpty()) ? simpleDateFormat.parse(probationEndDate) : null);
            employee.setLeaveActivationDate((leaveActivationDate != null && !leaveActivationDate.trim().isEmpty()) ? simpleDateFormat.parse(leaveActivationDate) : null);
            employeeRepo.save(employee);
            activityService.createSimpleActivity("SYSTEM", employee.getEmployeeId(), "ABI PPL UPDATES EMPLOYEE DATA", "ENROLLED: " + enrolledDate + separator + "LEAVEACTIVATION: " + leaveActivationDate + separator + "CONTRACT: " + contractDate + separator + "PROBATION: " + probationEndDate, "SYSTEM");
            return GeneralResponseUtil.resultSuccessWithData(employee);
        }
        return GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getEmployeeLeaveTypesById(long id) {
        Optional<Employee> employeeOptional = employeeRepo.findById(id);
        if (employeeOptional.isPresent()) {
            List<LeaveType> leaveTypes = employeeOptional.get().getGroupPayroll().getLeaveTypes();
            leaveTypes.add(employeeOptional.get().getGroupPayroll().getDefaultLeaveType());
            if (leaveTypes != null) {
                return GeneralResponseUtil.resultSuccessWithData(leaveTypes);
            }
        }
        return GeneralResponseUtil.resultFailureNoData();
    }
}
