package com.abhitech.leave.service;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.bean.ReportBalanceBean;
import com.abhitech.leave.bean.ReportScheduleBean;
import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.LeaveBalance;
import com.abhitech.leave.model.LeaveRequest;
import com.abhitech.leave.model.LeaveType;
import com.abhitech.leave.repository.EmployeeRepo;
import com.abhitech.leave.repository.LeaveBalanceRepo;
import com.abhitech.leave.repository.LeaveRequestRepo;
import com.abhitech.leave.repository.LeaveTypeRepo;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReportingService {
    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    LeaveBalanceRepo leaveBalanceRepo;

    @Autowired
    LeaveRequestRepo leaveRequestRepo;

    @Autowired
    LeaveTypeRepo leaveTypeRepo;

    public GeneralResponse generateReportBalance() {
        List<ReportBalanceBean> reportBalanceBeans = new ArrayList<>();
        Iterable<Employee> employees = employeeRepo.findAll();
        for (Employee employee : employees) {
            List<LeaveBalance> leaveBalances = leaveBalanceRepo.findByEmployee(employee);
            for (LeaveBalance leaveBalance : leaveBalances) {
                Integer usedBalance = leaveRequestRepo.sumLeaveAmountByLeaveBalanceId(leaveBalance.getId());
                ReportBalanceBean rb = new ReportBalanceBean();
                rb.setEmployeeId(employee.getEmployeeId());
                rb.setEmployeeName(employee.getEmployeeName());
                rb.setProjectName(employee.getProject());
                rb.setGroupPayrollName(employee.getGroupPayroll().getGroupName());
                rb.setGroupCompany(employee.getGroupPayroll().getProject().getGroupCompany());
                rb.setClient(employee.getGroupPayroll().getProject().getClient().getClientName());
                rb.setDepartment(employee.getDepartmentName());
                rb.setJobTitle(employee.getJobTitle());
                rb.setEnrolledDate(employee.getDateEnrolled());
                rb.setContractStartDate(employee.getContractStartDate());
                rb.setContractEndDate(employee.getContractEndDate());
                rb.setIsActive(employee.isIsActive() ? "ACTIVE" : "INACTIVE");
                rb.setLeaveTypeName(leaveBalance.getLeaveType().getLeaveName());
                rb.setLeaveBalanceId(leaveBalance.getId());
                rb.setDefaultLeaveBalance(leaveBalance.getLeaveType().getDefaultBalance());
                rb.setLeaveBalanceCreatedDate(leaveBalance.getCreatedDate());
                rb.setLeaveBalanceExpiredDate(leaveBalance.getExpiredDate());
                rb.setLeaveBalanceStatus(leaveBalance.isStatus() ? "ACTIVE" : "INACTIVE");
                rb.setLeaveBalanceUsed(usedBalance != null ? usedBalance : 0);
                rb.setLeaveBalanceRemaining(leaveBalance.getBalance());
                reportBalanceBeans.add(rb);
            }
        }
        return GeneralResponseUtil.resultSuccessWithData(reportBalanceBeans);
    }

    public GeneralResponse generateReportSchedule() {
        List<ReportScheduleBean> reportScheduleBeans = new ArrayList<>();
        Iterable<LeaveRequest> leaveRequests = leaveRequestRepo.findAll();
        for (LeaveRequest leaveRequest : leaveRequests) {
            Optional<Employee> employeeOptional = employeeRepo.findById(leaveRequest.getEmployee().getId());
            Optional<LeaveType> leaveTypeOptional = leaveTypeRepo.findById(leaveRequest.getLeaveType().getId());
            if (employeeOptional.isPresent() && leaveTypeOptional.isPresent()) {
                Employee employee = employeeOptional.get();
                LeaveType leaveType = leaveTypeOptional.get();
                ReportScheduleBean rs = new ReportScheduleBean();
                rs.setEmployeeId(employee.getEmployeeId());
                rs.setEmployeeName(employee.getEmployeeName());
                rs.setProjectName(employee.getProject());
                rs.setGroupPayrollName(employee.getGroupPayroll().getGroupName());
                rs.setGroupCompany(employee.getGroupPayroll().getProject().getGroupCompany());
                rs.setClient(employee.getGroupPayroll().getProject().getClient().getClientName());
                rs.setDepartment(employee.getDepartmentName());
                rs.setJobTitle(employee.getJobTitle());
                rs.setContractStartDate(employee.getContractStartDate());
                rs.setContractEndDate(employee.getContractEndDate());
                rs.setIsActive(employee.isIsActive() ? "ACTIVE" : "INACTIVE");
                rs.setLeaveTypeName(leaveType.getLeaveName());
                rs.setLeaveBalanceId(leaveRequest.getLeaveBalance().getId());
                rs.setLeaveBalanceCreatedDate(leaveRequest.getLeaveBalance().getCreatedDate());
                rs.setLeaveRequestCreatedDate(leaveRequest.getCreatedDate());
                rs.setStartDate(leaveRequest.getDateFrom());
                rs.setEndDate(leaveRequest.getDateTo());
                rs.setDuration(leaveRequest.getAmount());
                rs.setReason(leaveRequest.getReason());
                rs.setRemainingBalance(leaveRequest.getBalanceAfter());
                reportScheduleBeans.add(rs);
            }
        }
        return GeneralResponseUtil.resultSuccessWithData(reportScheduleBeans);
    }

    public GeneralResponse findAllScheduleByParams(String leaveName, String startDate, String endDate) {
        List<ReportScheduleBean> reportScheduleBeans = new ArrayList<>();
        Iterable<LeaveRequest> leaveRequests = leaveRequestRepo.findAllByDateFromAndDateTo(startDate,endDate);
        for (LeaveRequest leaveRequest : leaveRequests) {
            Optional<Employee> employeeOptional = employeeRepo.findById(leaveRequest.getEmployee().getId());
            Optional<LeaveType> leaveTypeOptional = leaveTypeRepo.findByIdAndLeaveName(leaveRequest.getLeaveType().getId(),leaveName);
            if (employeeOptional.isPresent() && leaveTypeOptional.isPresent()) {
                Employee employee = employeeOptional.get();
                LeaveType leaveType = leaveTypeOptional.get();
                ReportScheduleBean rs = new ReportScheduleBean();
                rs.setEmployeeId(employee.getEmployeeId());
                rs.setEmployeeName(employee.getEmployeeName());
                rs.setProjectName(employee.getProject());
                rs.setGroupPayrollName(employee.getGroupPayroll().getGroupName());
                rs.setGroupCompany(employee.getGroupPayroll().getProject().getGroupCompany());
                rs.setClient(employee.getGroupPayroll().getProject().getClient().getClientName());
                rs.setDepartment(employee.getDepartmentName());
                rs.setJobTitle(employee.getJobTitle());
                rs.setContractStartDate(employee.getContractStartDate());
                rs.setContractEndDate(employee.getContractEndDate());
                rs.setIsActive(employee.isIsActive() ? "ACTIVE" : "INACTIVE");
                rs.setLeaveTypeName(leaveType.getLeaveName());
                rs.setLeaveBalanceId(leaveRequest.getLeaveBalance().getId());
                rs.setLeaveBalanceCreatedDate(leaveRequest.getLeaveBalance().getCreatedDate());
                rs.setLeaveRequestCreatedDate(leaveRequest.getCreatedDate());
                rs.setStartDate(leaveRequest.getDateFrom());
                rs.setEndDate(leaveRequest.getDateTo());
                rs.setDuration(leaveRequest.getAmount());
                rs.setReason(leaveRequest.getReason());
                rs.setRemainingBalance(leaveRequest.getBalanceAfter());
                reportScheduleBeans.add(rs);
            }
        }
        return GeneralResponseUtil.resultSuccessWithData(reportScheduleBeans);
    }

    public GeneralResponse findAllBalanceByParams(String employeeId, String employeeName,
                                                  String projectCode, String projectName, String clientName, String clientCode, String groupCompany) {
        List<ReportBalanceBean> reportBalanceBeans = new ArrayList<>();
        Iterable<Employee> employees = employeeRepo.findAllEmployeeByParams(employeeId, employeeName, projectCode, projectName, clientName, clientCode, groupCompany);
        for (Employee employee : employees) {
            List<LeaveBalance> leaveBalances = leaveBalanceRepo.findByEmployee(employee);
            for (LeaveBalance leaveBalance : leaveBalances) {
                Integer usedBalance = leaveRequestRepo.sumLeaveAmountByLeaveBalanceId(leaveBalance.getId());
                ReportBalanceBean rb = new ReportBalanceBean();
                rb.setEmployeeId(employee.getEmployeeId());
                rb.setEmployeeName(employee.getEmployeeName());
                rb.setProjectName(employee.getProject());
                rb.setGroupPayrollName(employee.getGroupPayroll().getGroupName());
                rb.setGroupCompany(employee.getGroupPayroll().getProject().getGroupCompany());
                rb.setClient(employee.getGroupPayroll().getProject().getClient().getClientName());
                rb.setDepartment(employee.getDepartmentName());
                rb.setJobTitle(employee.getJobTitle());
                rb.setEnrolledDate(employee.getDateEnrolled());
                rb.setContractStartDate(employee.getContractStartDate());
                rb.setContractEndDate(employee.getContractEndDate());
                rb.setIsActive(employee.isIsActive() ? "ACTIVE" : "INACTIVE");
                rb.setLeaveTypeName(leaveBalance.getLeaveType().getLeaveName());
                rb.setLeaveBalanceId(leaveBalance.getId());
                rb.setDefaultLeaveBalance(leaveBalance.getLeaveType().getDefaultBalance());
                rb.setLeaveBalanceCreatedDate(leaveBalance.getCreatedDate());
                rb.setLeaveBalanceExpiredDate(leaveBalance.getExpiredDate());
                rb.setLeaveBalanceStatus(leaveBalance.isStatus() ? "ACTIVE" : "INACTIVE");
                rb.setLeaveBalanceUsed(usedBalance != null ? usedBalance : 0);
                rb.setLeaveBalanceRemaining(leaveBalance.getBalance());
                reportBalanceBeans.add(rb);
            }
        }
        return GeneralResponseUtil.resultSuccessWithData(reportBalanceBeans);
    }

}
