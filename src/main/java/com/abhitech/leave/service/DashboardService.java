package com.abhitech.leave.service;

import com.abhitech.leave.bean.DashboardBean;
import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.model.LeaveRequest;
import com.abhitech.leave.repository.EmployeeRepo;
import com.abhitech.leave.repository.LeaveBalanceRepo;
import com.abhitech.leave.repository.LeaveRequestRepo;
import com.abhitech.leave.repository.ProjectRepo;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class DashboardService {
    @Autowired
    ProjectRepo projectRepo;
    @Autowired
    LeaveBalanceRepo leaveBalanceRepo;
    @Autowired
    LeaveRequestRepo leaveRequestRepo;
    @Autowired
    EmployeeRepo employeeRepo;

    public GeneralResponse generateDashboard() {
        DashboardBean dashboardBean = new DashboardBean();
        dashboardBean.setTotalProjectsActive(projectRepo.countAllByEnable(true));
        dashboardBean.setTotalLeaveBalancesActive(leaveBalanceRepo.sumBalanceFromLeaveBalanceActive());
        dashboardBean.setTotalLeaveRequests(leaveRequestRepo.sumLeaveRequestByDate(LocalDate.now(), LocalDate.now()));
        dashboardBean.setTotalPersonnelActive(employeeRepo.countAllByIsActive(true));
        dashboardBean.setTotalPersonnelOnLeave(leaveRequestRepo.countDistinctByEmployeeAndTodayDate(LocalDate.now()));
        dashboardBean.setTotalPersonnelNegativeBalance(leaveBalanceRepo.countDistinctEmployeeByBalanceNegative());
        return GeneralResponseUtil.resultSuccessWithData(dashboardBean);
    }

    public GeneralResponse generateLeaveRequestChart(String employeeId, String projectCode, String leaveCode, @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateFrom, @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateTo) {
        List<LeaveRequest> result = leaveRequestRepo.findAllByStatusOrderByCreatedDateDesc("SUBMITTED");
        if (result.size() > 0)
            return GeneralResponseUtil.resultSuccessWithData(result);
        return GeneralResponseUtil.resultFailureNoData();
    }
}
