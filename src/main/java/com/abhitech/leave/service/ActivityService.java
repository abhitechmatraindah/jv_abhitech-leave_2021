package com.abhitech.leave.service;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.model.Activity;
import com.abhitech.leave.repository.ActivityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ActivityService {

    @Autowired
    ActivityRepo activityRepo;

    @Async
    public void createSimpleActivity(String fromUserId, String toUserId, String type, String description, String value, String status, String role) {
        Activity activity = new Activity();
        activity.setFromUserId(fromUserId);
        activity.setCreatedDate(new Date());
        activity.setToUserId(toUserId);
        activity.setType(type);
        activity.setDescription(description);
        activity.setValue(value);
        activity.setLastUpdateDate(new Date());
        activity.setStatus(status);
        activity.setRole(role);
        activityRepo.save(activity);
    }

    @Async
    public void createSimpleActivity(String fromUserId, String toUserId, String description, String value, String role) {
        Activity activity = new Activity();
        activity.setFromUserId(fromUserId);
        activity.setCreatedDate(new Date());
        activity.setToUserId(toUserId);
        activity.setDescription(description);
        activity.setValue(value);
        activity.setLastUpdateDate(new Date());
        activity.setRole(role);
        activityRepo.save(activity);
    }

    @Async
    public void createActivity(String fromUserId, String toUserId, String type, String description, String refId,
                               String refType, String value, String status, String role, String remarks) {
        Activity activity = new Activity();
        activity.setFromUserId(fromUserId);
        activity.setCreatedDate(new Date());
        activity.setToUserId(toUserId);
        activity.setType(type);
        if (refId != null && !refId.isEmpty()) {
            activity.setReferenceId(refId);
            activity.setReferenceType(refType);
        }
        activity.setDescription(description);
        activity.setValue(value);
        activity.setLastUpdateDate(new Date());
        activity.setStatus(status);
        activity.setRole(role);
        activity.setRemarks(remarks);
        activityRepo.save(activity);
    }

    public GeneralResponse getAllActivities() {
        Iterable<Activity> result = activityRepo.findAll();
        if (result != null)
            return com.abhitech.leave.util.GeneralResponseUtil.resultSuccessWithData(result);
        return com.abhitech.leave.util.GeneralResponseUtil.resultFailureNoData();
    }

    public GeneralResponse getAllActivitiesByParams(String createdDate, String lastUpdateDate, String fromUserId, String toUserId) {
        Iterable<Activity> result = activityRepo.findAllActivityByParams(createdDate, lastUpdateDate, fromUserId, toUserId);
        if (result != null)
            return com.abhitech.leave.util.GeneralResponseUtil.resultSuccessWithData(result);
        return com.abhitech.leave.util.GeneralResponseUtil.resultFailureNoData();
    }
}
