package com.abhitech.leave.service;

import com.abhitech.leave.bean.*;
import com.abhitech.leave.model.GroupPayroll;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class ApiService {
    // PAYROLL 172.20.2.36
    String serverIp = "http://172.20.2.31";
    String serverPath = serverIp + ":8181";

    public ProjectResponse getAllProject() {
        RestTemplate restTemplate = new RestTemplate();
        ProjectResponse projectResponse = restTemplate.getForObject(serverPath + "/getAllProject", ProjectResponse.class);
        return projectResponse;
    }

    public ClientResponse getAllClient() {
        RestTemplate restTemplate = new RestTemplate();
        ClientResponse clientResponse = restTemplate.getForObject(serverPath + "/getAllClient", ClientResponse.class);
        return clientResponse;
    }

    public HolidayResponse getAllHoliday() {
        RestTemplate restTemplate = new RestTemplate();
        HolidayResponse holidayResponse = restTemplate.getForObject(serverPath + "/getAllHoliday", HolidayResponse.class);
        return holidayResponse;
    }

    public GroupPayrollResponse getGroupPayrollByProjectId(Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        GroupPayroll[] gps = restTemplate.getForObject(serverIp + ":8183" + "/getGroupPayrollByProjectId/" + id, GroupPayroll[].class);
        List<GroupPayroll> groupPayrollList = Arrays.asList(gps);
        GroupPayrollResponse groupPayrollResponse = new GroupPayrollResponse();
        groupPayrollResponse.setMessage("Success");
        groupPayrollResponse.setResult(groupPayrollList);
        return groupPayrollResponse;
    }

    public EmployeeResponse getEmployeeByGroupPayrollId(String id) {
        RestTemplate restTemplate = new RestTemplate();
        EmployeeResponse employeeResponse = restTemplate.getForObject(serverPath + "/getEmployeeByGroupPayrollId/" + id, EmployeeResponse.class);
        return employeeResponse;
    }
}
