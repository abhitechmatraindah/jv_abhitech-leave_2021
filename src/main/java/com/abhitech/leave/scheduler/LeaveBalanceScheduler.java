package com.abhitech.leave.scheduler;

import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.GroupPayroll;
import com.abhitech.leave.model.LeaveBalance;
import com.abhitech.leave.model.LeaveType;
import com.abhitech.leave.repository.EmployeeRepo;
import com.abhitech.leave.repository.GroupPayrollRepo;
import com.abhitech.leave.repository.LeaveBalanceRepo;
import com.abhitech.leave.service.ApiService;
import com.abhitech.leave.util.LeaveTypeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class LeaveBalanceScheduler {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    GroupPayrollRepo groupPayrollRepo;

    @Autowired
    LeaveBalanceRepo leaveBalanceRepo;

    @Autowired
    ApiService apiService;

    // ONLY RUN ONCE AT FIRST DEPLOYMENT!!
//    public void firstInitiateLeaveBalances() {
//        logger.info("===== BEGIN INITIATE EMPLOYEE LEAVEBALANCE =====");
//        Iterable<Employee> employees = employeeRepo.findAll();
//        for (Employee employee : employees) {
//            GroupPayroll groupPayroll = employee.getGroupPayroll();
//            if (groupPayroll != null) {
//                LeaveType defaultLeaveType = groupPayroll.getDefaultLeaveType();
//                List<LeaveType> leaveTypes = groupPayroll.getLeaveTypes();
//                if (defaultLeaveType != null && !defaultLeaveType.isExpired()) {
//                    initiateEmployeeLeaveBalances(groupPayroll, employee, defaultLeaveType);
//                }
//                if (leaveTypes != null) {
//                    for (LeaveType leaveType : leaveTypes) {
//                        if (!leaveType.isExpired()) {
//                            initiateEmployeeLeaveBalances(groupPayroll, employee, leaveType);
//                        }
//                    }
//                }
//            }
//            logger.info("===== END INITIATE EMPLOYEE LEAVEBALANCE =====");
//        }
//    }
//    @Scheduled(fixedDelay = 10000000)
    @Scheduled(cron = "0 0 3 * * ?")
    public void initiateLeaveBalances() {
        logger.info("===== BEGIN INITIATE EMPLOYEE LEAVEBALANCE =====");
        Iterable<Employee> employees = employeeRepo.findAll();
        for (Employee employee : employees) {
            GroupPayroll groupPayroll = employee.getGroupPayroll();
            List<LeaveBalance> leaveBalances = leaveBalanceRepo.findByEmployeeAndStatusAndExpired(employee, true, false);
            List<String> leaveCodeFromLeavebalance = new ArrayList<>();
            for (LeaveBalance leaveBalance : leaveBalances) {
                leaveCodeFromLeavebalance.add(leaveBalance.getLeaveType().getLeaveCode());
            }
            if (groupPayroll != null) {
                LeaveType defaultLeaveType = groupPayroll.getDefaultLeaveType();
                List<LeaveType> leaveTypes = groupPayroll.getLeaveTypes();
                if (defaultLeaveType != null && !defaultLeaveType.isExpired()) {
                    if (!leaveCodeFromLeavebalance.contains(defaultLeaveType.getLeaveCode())) {
                        logger.info("=========DEFAULT LEAVETYPE===========");
                        logger.info("LBAL CONTAINS LCODE: " + leaveCodeFromLeavebalance.contains(defaultLeaveType.getLeaveCode()));
                        logger.info("LCODE: " + defaultLeaveType.getLeaveCode());
                        initiateEmployeeLeaveBalances(groupPayroll, employee, defaultLeaveType);
                    }
                }
                if (leaveTypes != null) {
                    for (LeaveType leaveType : leaveTypes) {
                        if (!leaveType.isExpired()) {
                            if (!leaveCodeFromLeavebalance.contains(leaveType.getLeaveCode())) {
                                logger.info("=========LEAVETYPE===========");
                                logger.info("LCODE from LBAL: " + leaveCodeFromLeavebalance);
                                logger.info("LCODE: " + defaultLeaveType.getLeaveCode());
                                initiateEmployeeLeaveBalances(groupPayroll, employee, leaveType);
                            }
                        }
                    }
                }
            }
        }
        logger.info("===== END INITIATE EMPLOYEE LEAVEBALANCE =====");
    }

    @Async
    void initiateEmployeeLeaveBalances(GroupPayroll groupPayroll, Employee employee, LeaveType leaveType) {
        try {
            if (leaveType.getGender().equalsIgnoreCase("ALL") || leaveType.getGender().equalsIgnoreCase(employee.getGender())) {
                LeaveBalance leaveBalance = new LeaveBalance();
                leaveBalance.setGroupPayroll(groupPayroll);
                leaveBalance.setEmployee(employee);
                leaveBalance.setLeaveType(leaveType);
                leaveBalance.setBalance(leaveType.getDefaultBalance());
                leaveBalance.setNextAddDate(LeaveTypeUtil.getAddDateFromEmployeeAndLeaveType(leaveType, employee));
                leaveBalance.setNextRestartDate(LeaveTypeUtil.getRestartDateFromEmployeeAndLeaveType(leaveType, employee));
                leaveBalance.setExpiredDate(LeaveTypeUtil.getExpDateFromEmployeeAndLeaveType(leaveType, employee));
                leaveBalance.setLastUpdatedDate(new Date());
                logger.info("Initiating leaveBalance : " + groupPayroll.getGroupCode() + " || " + employee.getEmployeeName() + " || " + leaveType.getLeaveName());
                leaveBalanceRepo.save(leaveBalance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

