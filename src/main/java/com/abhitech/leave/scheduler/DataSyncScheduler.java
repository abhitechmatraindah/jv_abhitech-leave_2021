package com.abhitech.leave.scheduler;

import com.abhitech.leave.bean.ClientBean;
import com.abhitech.leave.bean.EmployeeBean;
import com.abhitech.leave.bean.HolidayBean;
import com.abhitech.leave.bean.ProjectBean;
import com.abhitech.leave.model.*;
import com.abhitech.leave.repository.*;
import com.abhitech.leave.service.ActivityService;
import com.abhitech.leave.service.ApiService;
import com.abhitech.leave.service.RemovedLeaveBalanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DataSyncScheduler {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ApiService apiService;

    @Autowired
    GroupPayrollRepo groupPayrollRepo;

    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    HolidayRepo holidayRepo;

    @Autowired
    LeaveBalanceRepo leaveBalanceRepo;

    @Autowired
    RemovedLeaveBalanceService removedLeaveBalanceService;

    @Autowired
    ActivityService activityService;

    @Autowired
    ClientRepo clientRepo;

    @Autowired
    ProjectRepo projectRepo;

    //        @Scheduled(fixedDelay = 10000000)
    @Scheduled(cron = "0 0 1 * * ?")
    public void synchronizeData() {
        logger.info("==============BEGIN SYNCHRONIZE DATA==============");
        List<ClientBean> clientBeans = new ArrayList<>();
        List<ProjectBean> projectBeans = new ArrayList<>();
        List<HolidayBean> holidayBeans = new ArrayList<>();
        try {
            clientBeans = apiService.getAllClient().getResult();
            projectBeans = apiService.getAllProject().getResult();
            holidayBeans = apiService.getAllHoliday().getResult();
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Unable to get data from payroll engine");
        }

        for (ClientBean clientBean : clientBeans) {
            Client existingClient = clientRepo.findByClientCode(clientBean.getClientCode());
            if (existingClient == null) {
                logger.info("Registering new client: " + clientBean.getClientName());
                Client client = new Client();
                client.setClientCode(clientBean.getClientCode());
                client.setClientName(clientBean.getClientName());
                client.setEnable(clientBean.getEnable());
                clientRepo.save(client);
            }
        }

        // BEGIN SYNCHRONIZE PROJECT
        for (ProjectBean projectBean : projectBeans) {
            logger.info("Project Code: " + projectBean.getProjectCode());
            Project existingProject = projectRepo.findByProjectCode(projectBean.getProjectCode());
            if (existingProject == null) {
                logger.info("Registering new project: " + projectBean.getProjectName());
                Client existingClient = clientRepo.findByClientCode(projectBean.getClient().getClientCode());
                Project project = new Project();
                if (existingClient != null) {
                    logger.info("Client exist: " + existingClient.getClientCode());
                    project.setClient(existingClient);
                }
                project.setEnable(projectBean.getEnable());
                project.setGroupCompany(projectBean.getGroupCompany());
                project.setProjectCode(projectBean.getProjectCode());
                project.setProjectName(projectBean.getProjectName());
                project.setProjectStartDate(projectBean.getProjectStartDate());
                project.setProjectEndDate(projectBean.getProjectEndDate());
                projectRepo.save(project);
            }
            List<GroupPayroll> groupPayrollList = apiService.getGroupPayrollByProjectId(projectBean.getId()).getResult();
            for (GroupPayroll gp : groupPayrollList) {
                logger.info("GroupPayroll: " + gp.getGroupCode());
                GroupPayroll groupPayroll = groupPayrollRepo.findByGroupCode(gp.getGroupCode());
                if (groupPayroll == null) {
                    Project project = projectRepo.findByProjectCode(gp.getProject().getProjectCode());
                    if (project != null) {
                        GroupPayroll newGroupPayroll = new GroupPayroll();
                        newGroupPayroll.setSecureId(gp.getSecureId());
                        newGroupPayroll.setGroupCode(gp.getGroupCode());
                        newGroupPayroll.setGroupName(gp.getGroupName());
                        newGroupPayroll.setProject(project);
                        logger.info("Saving new record: " + newGroupPayroll.getGroupName());
                        groupPayroll = groupPayrollRepo.save(newGroupPayroll);
                    }
                }
                List<EmployeeBean> employeeBeans = apiService.getEmployeeByGroupPayrollId(gp.getSecureId()).getResult();
                for (EmployeeBean employeeBean : employeeBeans) {
                    Employee employee = employeeRepo.findBySecureId(employeeBean.getId());
                    if (employee == null) {
                        Employee newEmployee = new Employee();
                        newEmployee.setGroupPayroll(groupPayroll);
                        newEmployee.setSecureId(employeeBean.getId());
                        newEmployee.setEmployeeId(employeeBean.getEmployee_id());
                        newEmployee.setEmployeeName(employeeBean.getEmployee_name());
                        newEmployee.setProject(employeeBean.getProject());
                        newEmployee.setLocation(employeeBean.getLocation());
                        newEmployee.setJobTitle(employeeBean.getJob_title());
                        newEmployee.setBasicSalary(employeeBean.getBasic_salary());
                        newEmployee.setDateEnrolled(employeeBean.getDate_enrolled());
                        newEmployee.setGender(employeeBean.getGender());
                        newEmployee.setTaxType(employeeBean.getTax_type());
                        newEmployee.setDepartmentName(employeeBean.getDepartment_name());
                        logger.info("Saving new record: " + newEmployee.getEmployeeName());
                        employeeRepo.save(newEmployee);
                    } else if (employee.getGroupPayroll() != groupPayroll) {
                        logger.info("Employee changed group payroll: " + employeeBean.getEmployee_name());
                        List<LeaveBalance> leaveBalances = leaveBalanceRepo.findByEmployeeAndStatusAndExpired(employee, true, false);
                        for (LeaveBalance leaveBalance : leaveBalances) {
                            if (leaveBalance.getGroupPayroll() == employee.getGroupPayroll()) {
                                try {
                                    activityService.createActivity("SYSTEM", leaveBalance.getEmployee().getEmployeeId(), "GROUP PAYROLL CHANGE", "USER " + leaveBalance.getEmployee().getEmployeeId() + " CHANGED GROUP PAYROLL FROM " + employee.getGroupPayroll().getGroupCode() + " TO " + groupPayroll.getGroupCode(), null, null, "APPROVED", "SETTLED", "SYSTEM", null);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                removeLeaveBalancesForGroupPayrollChange(leaveBalance);
                            }
                        }
                        employee.setGroupPayroll(groupPayroll);
                        employeeRepo.save(employee);
                    }
                }
            }
        }
        // BEGIN SYNCHRONIZE HOLIDAY
        for (HolidayBean holidayBean : holidayBeans) {
            Holiday existingHoliday = holidayRepo.findBySecureId(holidayBean.getId());
            if (existingHoliday != null) {
                existingHoliday.setType(holidayBean.getType());
                existingHoliday.setNote(holidayBean.getNote());
                existingHoliday.setEnable(holidayBean.getEnable());
                existingHoliday.setHoliday(holidayBean.getHoliday());
                holidayRepo.save(existingHoliday);
            } else {
                Holiday holiday = new Holiday();
                holiday.setSecureId(holidayBean.getId());
                holiday.setHoliday(holidayBean.getHoliday());
                holiday.setEnable(holidayBean.getEnable());
                holiday.setNote(holidayBean.getNote());
                holiday.setType(holidayBean.getType());
                holidayRepo.save(holiday);
            }
        }
        logger.info("============== END SYNCHRONIZE DATA ==============");
    }

    private void removeLeaveBalancesForGroupPayrollChange(LeaveBalance leaveBalance) {
        removedLeaveBalanceService.addRemovedLeaveBalance(leaveBalance);
    }

    //    @Scheduled(fixedDelay = 10000000)
    public void updateData() {
        Iterable<GroupPayroll> groupPayrolls = groupPayrollRepo.findAll();
        for (GroupPayroll groupPayroll : groupPayrolls) {
            //CHECK IF GROUPPAYROLL STILL EXIST IN SERVER
        }
    }

}
