package com.abhitech.leave.scheduler;

import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.LeaveBalance;
import com.abhitech.leave.model.LeaveType;
import com.abhitech.leave.repository.EmployeeRepo;
import com.abhitech.leave.repository.GroupPayrollRepo;
import com.abhitech.leave.repository.LeaveBalanceRepo;
import com.abhitech.leave.repository.LeaveTypeRepo;
import com.abhitech.leave.util.LeaveTypeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Component
public class LeaveTypeScheduler {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    GroupPayrollRepo groupPayrollRepo;

    @Autowired
    LeaveBalanceRepo leaveBalanceRepo;

    @Autowired
    LeaveTypeRepo leaveTypeRepo;

    //    @Scheduled(fixedDelay = 10000000)
    @Scheduled(cron = "0 0 6 * * ?")
    public void addTypeScheduler() throws ParseException {
        logger.info("===== BEGIN PERIOD ADD LEAVE TYPE =====");
        List<LeaveBalance> leaveBalances = leaveBalanceRepo.findByNextAddDateBefore(new Date());
        logger.info("COUNT LEAVE BALANCE: " + leaveBalances.size());
        for (LeaveBalance leaveBalance : leaveBalances) {
            if (leaveBalance.getEmployee().getContractEndDate() != null && leaveBalance.getEmployee().getContractEndDate().after(new Date())) {
                if (!leaveBalance.isExpired() && leaveBalance.getEmployee().isIsActive() && !leaveBalance.getLeaveType().isExpired()) {
                    logger.info("ADD LEAVE BALANCE: " + leaveBalance.getId());
                    addEmployeeLeaveBalance(leaveBalance);
                }
            }
        }
        logger.info("===== END PERIOD ADD LEAVE TYPE =====");
    }

    //    @Scheduled(fixedDelay = 10000000)
    @Scheduled(cron = "0 0 4 * * ?")
    public void restartTypeScheduler() throws ParseException {
        logger.info("===== BEGIN PERIOD RESTART LEAVE TYPE =====");
        List<LeaveBalance> leaveBalances = leaveBalanceRepo.findByNextRestartDateBefore(new Date());
        logger.info("COUNT LEAVE BALANCES TO RESTART: " + leaveBalances.size());
        for (LeaveBalance leaveBalance : leaveBalances) {
            if (leaveBalance.getEmployee().getContractEndDate() != null && leaveBalance.getEmployee().getContractEndDate().after(new Date())) {
                if (!leaveBalance.isExpired() && leaveBalance.getEmployee().isIsActive() && !leaveBalance.getLeaveType().isExpired()) {
                    logger.info("RESTART LEAVE BALANCE: " + leaveBalance.getId());
                    restartEmployeeLeaveBalance(leaveBalance);
                }
            }
        }
        logger.info("===== END PERIOD RESTART LEAVE TYPE =====");
    }

    @Async
    void restartEmployeeLeaveBalance(LeaveBalance oldLeaveBalance) {
        LeaveType leaveType = oldLeaveBalance.getLeaveType();
        Employee employee = oldLeaveBalance.getEmployee();
        logger.info("Creating leave balance for employee: " + employee.getEmployeeName());
        if (leaveType.getGender().equalsIgnoreCase("ALL") || leaveType.getGender().equalsIgnoreCase(employee.getGender())) {
            LeaveBalance newLeaveBalance = new LeaveBalance();
            newLeaveBalance.setGroupPayroll(oldLeaveBalance.getGroupPayroll());
            newLeaveBalance.setEmployee(employee);
            newLeaveBalance.setLeaveType(leaveType);
            newLeaveBalance.setBalance(leaveType.getDefaultBalance());
            newLeaveBalance.setLastUpdatedDate(new Date());
            newLeaveBalance.setLastRestartDate(new Date());
            newLeaveBalance.setNextAddDate(LeaveTypeUtil.getNextAddDateFromLeaveBalance(oldLeaveBalance));
            newLeaveBalance.setNextRestartDate(LeaveTypeUtil.getNextRestartDateFromLeaveBalance(oldLeaveBalance));
            newLeaveBalance.setExpiredDate(LeaveTypeUtil.getNextExpDateFromLeaveBalance(oldLeaveBalance));
            logger.info("Initiating employee leaveBalance : " + employee.getEmployeeName() + " || " + leaveType.getLeaveName());
            leaveBalanceRepo.save(newLeaveBalance);
        }
    }

    @Async
    void addEmployeeLeaveBalance(LeaveBalance leaveBalance) {
        LeaveType leaveType = leaveBalance.getLeaveType();
        if (leaveBalance != null) {
            if (leaveType.getMaxBalance() > 0) {
                if ((leaveBalance.getBalance() + leaveType.getAddAmount()) >= leaveType.getMaxBalance()) {
                    leaveBalance.setBalance(leaveType.getMaxBalance());
                } else {
                    leaveBalance.setBalance(leaveBalance.getBalance() + leaveType.getAddAmount());
                }
            } else {
                leaveBalance.setBalance(leaveBalance.getBalance() + leaveType.getAddAmount());
            }
            leaveBalance.setLastUpdatedDate(new Date());
            leaveBalance.setLastAddDate(new Date());
            leaveBalance.setNextAddDate(LeaveTypeUtil.getNextAddDateFromLeaveBalance(leaveBalance));
            leaveBalanceRepo.save(leaveBalance);
        }
    }

}
