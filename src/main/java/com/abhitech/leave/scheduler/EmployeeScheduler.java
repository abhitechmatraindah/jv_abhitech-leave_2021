package com.abhitech.leave.scheduler;

import com.abhitech.leave.bean.*;
import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.GroupPayroll;
import com.abhitech.leave.repository.EmployeeRepo;
import com.abhitech.leave.repository.GroupPayrollRepo;
import com.abhitech.leave.repository.LeaveBalanceRepo;
import com.abhitech.leave.service.ApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmployeeScheduler {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    GroupPayrollRepo groupPayrollRepo;

    @Autowired
    LeaveBalanceRepo leaveBalanceRepo;

    @Autowired
    ApiService apiService;

//    @Scheduled(fixedDelay = 10000000)
    @Scheduled(cron = "0 0 2 * * ?")
    public void updateEmployeeGroupPayroll() {
        logger.info("===== BEGIN UPDATE EMPLOYEE GROUPPAYROLL =====");
        ProjectResponse projectResponse = apiService.getAllProject();
        List<ProjectBean> projectBeans = projectResponse.getResult();
        for (ProjectBean projectBean : projectBeans) {
            logger.info("Project: " + projectBean.getProjectCode());
            GroupPayrollResponse groupPayrollResponse = apiService.getGroupPayrollByProjectId(projectBean.getId());
            List<GroupPayroll> groupPayrollList = groupPayrollResponse.getResult();
            for (GroupPayroll gp : groupPayrollList) {
                logger.info("GroupPayroll: " + gp.getGroupCode());
                GroupPayroll groupPayroll = groupPayrollRepo.findBySecureId(gp.getSecureId());
                if (groupPayroll != null) {
                    EmployeeResponse employeeResponse = apiService.getEmployeeByGroupPayrollId(gp.getSecureId());
                    List<EmployeeBean> employeeBeans = employeeResponse.getResult();
                    for (EmployeeBean employeeBean : employeeBeans) {
                        Employee employee = employeeRepo.findBySecureId(employeeBean.getId());
                        employee.setGroupPayroll(groupPayroll);
                        logger.info("Updating employee groupPayroll : " + employee.getEmployeeName() + " || " + groupPayroll.getGroupName());
                        employeeRepo.save(employee);
                    }
                }
            }
        }
        logger.info("===== END UPDATE EMPLOYEE GROUPPAYROLL =====");
    }
}
