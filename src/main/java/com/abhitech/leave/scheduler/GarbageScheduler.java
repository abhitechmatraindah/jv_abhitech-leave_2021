package com.abhitech.leave.scheduler;

import com.abhitech.leave.repository.LeaveRequestRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class GarbageScheduler {

    @Autowired
    LeaveRequestRepo leaveRequestRepo;

//    @Scheduled(cron = "0 0 1 * * ?")
    public void cleanPendingRequest() {
//        leaveRequestRepo.deleteByStatus();
    }

}
