package com.abhitech.leave.scheduler;

import com.abhitech.leave.model.LeaveBalance;
import com.abhitech.leave.model.LeaveRequest;
import com.abhitech.leave.model.LeaveType;
import com.abhitech.leave.repository.LeaveBalanceRepo;
import com.abhitech.leave.repository.LeaveRequestRepo;
import com.abhitech.leave.repository.LeaveTypeRepo;
import com.abhitech.leave.service.RemovedLeaveBalanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class ExpiredScheduler {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    LeaveRequestRepo leaveRequestRepo;

    @Autowired
    LeaveTypeRepo leaveTypeRepo;

    @Autowired
    LeaveBalanceRepo leaveBalanceRepo;

    @Autowired
    RemovedLeaveBalanceService removedLeaveBalanceService;


    //    @Scheduled(fixedDelay = 10000000)
    @Scheduled(cron = "0 0 0 * * ?")
    public void updateEndedLeaveRequest() {
        logger.info("RUNNING LEAVE REQUEST ENDED SCHEDULER");
        List<LeaveRequest> leaveRequests = leaveRequestRepo.findByDateToBefore(new Date());
        for (LeaveRequest leaveRequest : leaveRequests) {
            leaveRequest.setStatus("ENDED");
            leaveRequestRepo.save(leaveRequest);
        }
        logger.info("END LEAVE REQUEST ENDED SCHEDULER");
    }

    //    @Scheduled(fixedDelay = 10000000)
    @Scheduled(cron = "0 0 0 * * ?")
    public void updateExpiredLeaveBalance() {
        logger.info("RUNNING LEAVE BALANCE EXPIRED SCHEDULER");
        Iterable<LeaveBalance> leaveBalances = leaveBalanceRepo.findByExpiredDateBefore(new Date());
        for (LeaveBalance leaveBalance : leaveBalances) {
            if ((new Date()).after(leaveBalance.getExpiredDate()) && !leaveBalance.isExpired()) {
                logger.info("UPDATING EXPIRED LEAVE BALANCE: " + leaveBalance.getId());
                leaveBalance.setExpired(true);
                leaveBalance.setStatus(false);
                removedLeaveBalanceService.addRemovedLeaveBalance(leaveBalance);
                leaveBalanceRepo.save(leaveBalance);
            }
        }
        logger.info("END LEAVE BALANCE EXPIRED SCHEDULER");
    }

    //    @Scheduled(fixedDelay = 10000000)
    @Scheduled(cron = "0 0 0 * * ?")
    public void updateExpiredLeaveType() {
        logger.info("RUNNING LEAVE TYPE EXPIRED SCHEDULER");
        Calendar todayCalendar = Calendar.getInstance();
        Iterable<LeaveType> leaveTypes = leaveTypeRepo.findByExpiredDateBefore(new Date());
        for (LeaveType leaveType : leaveTypes) {
            if ((new Date()).after(leaveType.getExpiredDate()) && !leaveType.isExpired()) {
                logger.info("UPDATING EXPIRED LEAVE TYPE: " + leaveType.getId());
                leaveType.setExpired(true);
                leaveType.setActive(false);
                leaveTypeRepo.save(leaveType);
            }
        }
        logger.info("END LEAVE TYPE EXPIRED SCHEDULER");
    }

}
