package com.abhitech.leave.bean;

public class DashboardBean {
    private int totalProjectsActive;
    private Long totalLeaveBalancesActive;
    private Long totalLeaveRequests;
    private Long totalPersonnelActive;
    private Long totalPersonnelOnLeave;
    private Long totalPersonnelNegativeBalance;

    public int getTotalProjectsActive() {
        return totalProjectsActive;
    }

    public void setTotalProjectsActive(int totalProjectsActive) {
        this.totalProjectsActive = totalProjectsActive;
    }

    public Long getTotalLeaveBalancesActive() {
        return totalLeaveBalancesActive;
    }

    public void setTotalLeaveBalancesActive(Long totalLeaveBalancesActive) {
        this.totalLeaveBalancesActive = totalLeaveBalancesActive;
    }

    public Long getTotalLeaveRequests() {
        return totalLeaveRequests;
    }

    public void setTotalLeaveRequests(Long totalLeaveRequests) {
        this.totalLeaveRequests = totalLeaveRequests;
    }

    public Long getTotalPersonnelActive() {
        return totalPersonnelActive;
    }

    public void setTotalPersonnelActive(Long totalPersonnelActive) {
        this.totalPersonnelActive = totalPersonnelActive;
    }

    public Long getTotalPersonnelOnLeave() {
        return totalPersonnelOnLeave;
    }

    public void setTotalPersonnelOnLeave(Long totalPersonnelOnLeave) {
        this.totalPersonnelOnLeave = totalPersonnelOnLeave;
    }

    public Long getTotalPersonnelNegativeBalance() {
        return totalPersonnelNegativeBalance;
    }

    public void setTotalPersonnelNegativeBalance(Long totalPersonnelNegativeBalance) {
        this.totalPersonnelNegativeBalance = totalPersonnelNegativeBalance;
    }
}
