package com.abhitech.leave.bean;

import com.abhitech.leave.model.GroupPayroll;

import java.util.List;

public class GroupPayrollResponse {
    private String message;
    private List<GroupPayroll> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GroupPayroll> getResult() {
        return result;
    }

    public void setResult(List<GroupPayroll> result) {
        this.result = result;
    }
}
