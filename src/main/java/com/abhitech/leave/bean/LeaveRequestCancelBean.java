package com.abhitech.leave.bean;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class LeaveRequestCancelBean {
    private long id;
    private int returnBalanceOption;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date leaveEndDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getReturnBalanceOption() {
        return returnBalanceOption;
    }

    public void setReturnBalanceOption(int returnBalanceOption) {
        this.returnBalanceOption = returnBalanceOption;
    }

    public Date getLeaveEndDate() {
        return leaveEndDate;
    }

    public void setLeaveEndDate(Date leaveEndDate) {
        this.leaveEndDate = leaveEndDate;
    }
}
