package com.abhitech.leave.bean;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

public class ReportBalanceBean {
    private String employeeId;
    private String employeeName;
    private String projectName;
    private String groupPayrollName;
    private String groupCompany;
    private String client;
    private String department;
    private String jobTitle;
    @Temporal(TemporalType.DATE)
    private Date enrolledDate;
    @Temporal(TemporalType.DATE)
    private Date contractStartDate;
    @Temporal(TemporalType.DATE)
    private Date contractEndDate;
    private String isActive;
    private String leaveTypeName;
    private Long leaveBalanceId;
    private Integer defaultLeaveBalance;
    @Temporal(TemporalType.DATE)
    private Date leaveBalanceCreatedDate;
    @Temporal(TemporalType.DATE)
    private Date leaveBalanceExpiredDate;
    private String leaveBalanceStatus;
    private Integer leaveBalanceUsed;
    private Integer leaveBalanceRemaining;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Date getEnrolledDate() {
        return enrolledDate;
    }

    public void setEnrolledDate(Date enrolledDate) {
        this.enrolledDate = enrolledDate;
    }

    public Date getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(Date contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public String getLeaveTypeName() {
        return leaveTypeName;
    }

    public void setLeaveTypeName(String leaveTypeName) {
        this.leaveTypeName = leaveTypeName;
    }

    public Long getLeaveBalanceId() {
        return leaveBalanceId;
    }

    public void setLeaveBalanceId(Long leaveBalanceId) {
        this.leaveBalanceId = leaveBalanceId;
    }

    public Integer getDefaultLeaveBalance() {
        return defaultLeaveBalance;
    }

    public void setDefaultLeaveBalance(Integer defaultLeaveBalance) {
        this.defaultLeaveBalance = defaultLeaveBalance;
    }

    public Date getLeaveBalanceCreatedDate() {
        return leaveBalanceCreatedDate;
    }

    public void setLeaveBalanceCreatedDate(Date leaveBalanceCreatedDate) {
        this.leaveBalanceCreatedDate = leaveBalanceCreatedDate;
    }

    public Date getLeaveBalanceExpiredDate() {
        return leaveBalanceExpiredDate;
    }

    public void setLeaveBalanceExpiredDate(Date leaveBalanceExpiredDate) {
        this.leaveBalanceExpiredDate = leaveBalanceExpiredDate;
    }

    public String getLeaveBalanceStatus() {
        return leaveBalanceStatus;
    }

    public void setLeaveBalanceStatus(String leaveBalanceStatus) {
        this.leaveBalanceStatus = leaveBalanceStatus;
    }

    public Integer getLeaveBalanceUsed() {
        return leaveBalanceUsed;
    }

    public void setLeaveBalanceUsed(Integer leaveBalanceUsed) {
        this.leaveBalanceUsed = leaveBalanceUsed;
    }

    public Integer getLeaveBalanceRemaining() {
        return leaveBalanceRemaining;
    }

    public void setLeaveBalanceRemaining(Integer leaveBalanceRemaining) {
        this.leaveBalanceRemaining = leaveBalanceRemaining;
    }

    public String getGroupPayrollName() {
        return groupPayrollName;
    }

    public void setGroupPayrollName(String groupPayrollName) {
        this.groupPayrollName = groupPayrollName;
    }

    public String getGroupCompany() {
        return groupCompany;
    }

    public void setGroupCompany(String groupCompany) {
        this.groupCompany = groupCompany;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Date getContractEndDate() {
        return contractEndDate;
    }

    public void setContractEndDate(Date contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}
