package com.abhitech.leave.bean;

import java.util.List;

public class ProjectResponse {
    private String message;
    private List<ProjectBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProjectBean> getResult() {
        return result;
    }

    public void setResult(List<ProjectBean> result) {
        this.result = result;
    }
}
