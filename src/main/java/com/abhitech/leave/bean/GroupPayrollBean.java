package com.abhitech.leave.bean;

public class GroupPayrollBean {
    private String id;
    private Boolean enable;
    private String group_code;
    private String group_name;
    private long default_leave_type_id;
    private String project;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getGroup_code() {
        return group_code;
    }

    public void setGroup_code(String group_code) {
        this.group_code = group_code;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public long getDefault_leave_type_id() {
        return default_leave_type_id;
    }

    public void setDefault_leave_type_id(long default_leave_type_id) {
        this.default_leave_type_id = default_leave_type_id;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

 }
