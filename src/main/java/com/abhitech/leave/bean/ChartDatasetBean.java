package com.abhitech.leave.bean;

import java.util.List;
import java.util.Random;

public class ChartDatasetBean {
    private final Random random = new Random();
    int r = random.nextInt(205) + 51;
    int g = random.nextInt(205) + 51;
    int b = random.nextInt(205) + 51;
    private String label;
    private List<Integer> data;
    private String[] backgroundColor = {"rgba(" + r + ", " + g + ", " + b + ", 0.5)",};
    private String[] borderColor = {"rgba(" + r + ", " + g + ", " + b + ",1)",};
    private int orderWidth = 1;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<Integer> getData() {
        return data;
    }

    public void setData(List<Integer> data) {
        this.data = data;
    }

    public String[] getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String[] backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String[] getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String[] borderColor) {
        this.borderColor = borderColor;
    }

    public int getOrderWidth() {
        return orderWidth;
    }

    public void setOrderWidth(int orderWidth) {
        this.orderWidth = orderWidth;
    }
}
