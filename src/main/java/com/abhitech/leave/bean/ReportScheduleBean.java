package com.abhitech.leave.bean;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

public class ReportScheduleBean {
    private String employeeId;
    private String employeeName;
    private String projectName;
    private String groupPayrollName;
    private String groupCompany;
    private String client;
    private String department;
    private String jobTitle;
    @Temporal(TemporalType.DATE)
    private Date contractStartDate;
    @Temporal(TemporalType.DATE)
    private Date contractEndDate;
    private String isActive;
    private String leaveTypeName;
    private Long leaveBalanceId;
    @Temporal(TemporalType.DATE)
    private Date leaveBalanceCreatedDate;
    @Temporal(TemporalType.DATE)
    private Date leaveRequestCreatedDate;
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Temporal(TemporalType.DATE)
    private Date endDate;
    private Integer duration;
    private String reason;
    private Integer remainingBalance;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getLeaveTypeName() {
        return leaveTypeName;
    }

    public void setLeaveTypeName(String leaveTypeName) {
        this.leaveTypeName = leaveTypeName;
    }

    public Long getLeaveBalanceId() {
        return leaveBalanceId;
    }

    public void setLeaveBalanceId(Long leaveBalanceId) {
        this.leaveBalanceId = leaveBalanceId;
    }

    public Date getLeaveBalanceCreatedDate() {
        return leaveBalanceCreatedDate;
    }

    public void setLeaveBalanceCreatedDate(Date leaveBalanceCreatedDate) {
        this.leaveBalanceCreatedDate = leaveBalanceCreatedDate;
    }

    public Date getLeaveRequestCreatedDate() {
        return leaveRequestCreatedDate;
    }

    public void setLeaveRequestCreatedDate(Date leaveRequestCreatedDate) {
        this.leaveRequestCreatedDate = leaveRequestCreatedDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getRemainingBalance() {
        return remainingBalance;
    }

    public void setRemainingBalance(Integer remainingBalance) {
        this.remainingBalance = remainingBalance;
    }

    public String getGroupPayrollName() {
        return groupPayrollName;
    }

    public void setGroupPayrollName(String groupPayrollName) {
        this.groupPayrollName = groupPayrollName;
    }

    public String getGroupCompany() {
        return groupCompany;
    }

    public void setGroupCompany(String groupCompany) {
        this.groupCompany = groupCompany;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Date getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(Date contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public Date getContractEndDate() {
        return contractEndDate;
    }

    public void setContractEndDate(Date contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}
