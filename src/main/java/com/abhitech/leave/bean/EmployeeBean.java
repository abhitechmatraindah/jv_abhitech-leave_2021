package com.abhitech.leave.bean;

import java.math.BigDecimal;
import java.util.Date;

public class EmployeeBean {
    private String id;
    private String employee_id;
    private String employee_name;
    private String project;
    private String location;
    private String job_title;
    private BigDecimal basic_salary;
    private Date date_enrolled;
    private String gender;
    private String tax_type;
    private String department_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public BigDecimal getBasic_salary() {
        return basic_salary;
    }

    public void setBasic_salary(BigDecimal basic_salary) {
        this.basic_salary = basic_salary;
    }

    public Date getDate_enrolled() {
        return date_enrolled;
    }

    public void setDate_enrolled(Date date_enrolled) {
        this.date_enrolled = date_enrolled;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTax_type() {
        return tax_type;
    }

    public void setTax_type(String tax_type) {
        this.tax_type = tax_type;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }
}
