package com.abhitech.leave.bean;

import java.util.List;

public class HolidayResponse {
    private String message;
    private List<HolidayBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<HolidayBean> getResult() {
        return result;
    }

    public void setResult(List<HolidayBean> result) {
        this.result = result;
    }
}
