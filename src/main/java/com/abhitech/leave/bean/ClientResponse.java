package com.abhitech.leave.bean;

import java.util.List;

public class ClientResponse {
    private String message;
    private List<ClientBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ClientBean> getResult() {
        return result;
    }

    public void setResult(List<ClientBean> result) {
        this.result = result;
    }
}
