package com.abhitech.leave.bean;

import java.util.List;

public class ChartBean {
    private List<String> labels;
    private List<ChartDatasetBean> datasets;

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public List<ChartDatasetBean> getDatasets() {
        return datasets;
    }

    public void setDatasets(List<ChartDatasetBean> datasets) {
        this.datasets = datasets;
    }
}
