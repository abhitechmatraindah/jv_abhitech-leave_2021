package com.abhitech.leave.bean;

import java.util.List;

public class EmployeeResponse {
    private String message;
    private List<EmployeeBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<EmployeeBean> getResult() {
        return result;
    }

    public void setResult(List<EmployeeBean> result) {
        this.result = result;
    }
}
