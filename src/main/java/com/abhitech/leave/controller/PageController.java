package com.abhitech.leave.controller;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.repository.ProjectRepo;
import com.abhitech.leave.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
public class PageController {
    @Autowired
    LeaveTypeService leaveTypeService;
    @Autowired
    ApiService apiService;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    LeaveRequestService leaveRequestService;
    @Autowired
    ProjectRepo projectRepo;
    @Autowired
    DashboardService dashboardService;
    @Autowired
    GroupPayrollService groupPayrollService;
    @Autowired
    ProjectService projectService;
    @Autowired
    ChartService chartService;
    @Autowired
    ActivityService activityService;
    @Autowired
    RemovedLeaveBalanceService removedLeaveBalanceService;
    @Autowired
    UserService userService;
    @Autowired
    ReportingService reportingService;
    @Autowired
    ClientSevice clientSevice;

    @RequestMapping(value = {"", "/", "/index"})
    public ModelAndView index(HttpServletRequest httpServletRequest) throws Exception {
        if (userService.isUserValid(httpServletRequest)) {
            ModelAndView modelAndView = new ModelAndView("index");
            modelAndView
                    .addObject("generalResponse", dashboardService.generateDashboard())
                    .addObject("chartData", chartService.generateLeaveRequestChart10Days());
            return modelAndView;
        } else {
            return new ModelAndView("login");
        }
    }

    @RequestMapping(value = "/login")
    public ModelAndView login(HttpServletRequest httpServletRequest, String username, String password) throws Exception {
        if (userService.validateUser(httpServletRequest, username, password)) {
            ModelAndView modelAndView = new ModelAndView("index").addObject("generalResponse", dashboardService.generateDashboard())
                    .addObject("chartData", chartService.generateLeaveRequestChart10Days());
            return modelAndView;
        } else {
            return new ModelAndView("login");
        }
    }

    @RequestMapping(value = "/logout")
    public ModelAndView logout(HttpServletRequest httpServletRequest) {
        userService.invalidateUser(httpServletRequest);
        return new ModelAndView("login");
    }

    @RequestMapping(value = "/leave_type_create")
    public ModelAndView formLeaveTypeCreate(HttpServletRequest httpServletRequest) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        return new ModelAndView("leavetype_create");
    }

    @RequestMapping(value = "/leave_type_manage")
    public ModelAndView formLeaveTypeManage(HttpServletRequest httpServletRequest) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        return new ModelAndView("leavetype_manage").addObject("generalResponse", leaveTypeService.getAllLeaveType());
    }

    /*@RequestMapping(value = "/testObject")
    @ResponseBody
    public GeneralResponse testObject(String leaveName, String from, String to){
        return reportingService.findAllScheduleByParams(leaveName,from,to);
    }*/

    @RequestMapping(value = "/leave_request_create")
    public ModelAndView formLeaveRequestCreate(HttpServletRequest httpServletRequest,String employeeId, String employeeName,
                                               String projectCode, String projectName, String clientName, String clientCode, String groupCompany) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        GeneralResponse generalResponse = employeeService.findAllEmployeeByParams(employeeId,employeeName,projectCode,projectName,clientName,clientCode,groupCompany);
        HashMap<String, GeneralResponse> passedObject = getQueryList();
        passedObject.put("generalResponse", generalResponse);

        return new ModelAndView("leaverequest_create").addAllObjects(passedObject);
    }

    @RequestMapping(value = "/leave_request_manage")
    public ModelAndView formLeaveRequestManage(HttpServletRequest httpServletRequest, String employeeId, String employeeName, String leaveName,
                                               String createdDate, String dateFrom, String dateTo, String projectCode, String projectName,
                                               String clientName, String clientCode, String groupCompany) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        GeneralResponse generalResponse = leaveRequestService.getAllLeaveRequestByParams("Submitted",employeeId,employeeName,leaveName,createdDate,dateFrom,dateTo,projectCode,projectName,clientName,clientCode,groupCompany);
        HashMap<String, GeneralResponse> passedObject = getQueryList();
        passedObject.put("generalResponse", generalResponse);
        return new ModelAndView("leave_request_manage").addAllObjects(passedObject);
    }

    @RequestMapping(value = "/bind_group_payroll")
    public ModelAndView formGroupPayrollBind(HttpServletRequest httpServletRequest) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        GeneralResponse generalResponse = projectService.getAllProject();
        return new ModelAndView("bind_grouppayroll").addObject("generalResponse", generalResponse);
    }

    @RequestMapping(value = "/grouppayroll_list")
    public ModelAndView groupPayrollList(Long id, HttpServletRequest httpServletRequest) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        httpServletRequest.getSession().setAttribute("generalResponse", leaveTypeService.getAllLeaveTypeEnabled());
        GeneralResponse generalResponse = groupPayrollService.getGroupPayrollByProjectId(id);
        return new ModelAndView("grouppayroll_list").addObject("generalResponse", generalResponse);
    }

    @GetMapping(value = "/employee_manage")
    public ModelAndView tableEmployeeManage(HttpServletRequest httpServletRequest,String employeeId, String employeeName,
                                            String projectCode, String projectName, String clientName, String clientCode, String groupCompany) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        GeneralResponse generalResponse = employeeService.findAllEmployeeByParams(employeeId,employeeName,projectCode,projectName,clientName,clientCode,groupCompany);
        HashMap<String, GeneralResponse> passedObject = getQueryList();
        passedObject.put("generalResponse", generalResponse);
    return new ModelAndView("employee_manage").addAllObjects(passedObject);
    }

    @RequestMapping(value = "/history_activity")
    public ModelAndView auditTrail(HttpServletRequest httpServletRequest,String createdDate, String lastUpdateDate, String fromUserId, String toUserId) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        GeneralResponse generalResponse = activityService.getAllActivitiesByParams(createdDate, lastUpdateDate, fromUserId, toUserId);
        return new ModelAndView("history_activity").addObject("generalResponse", generalResponse);
    }

    @RequestMapping(value = "/report_leave_balance")
    public ModelAndView reportingBalance(HttpServletRequest httpServletRequest,String employeeId, String employeeName,
                                         String projectCode, String projectName, String clientName, String clientCode, String groupCompany) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        GeneralResponse generalResponse = reportingService.findAllBalanceByParams(employeeId,employeeName,projectCode,projectName,clientName,clientCode,groupCompany);
        HashMap<String, GeneralResponse> passedObject = getQueryList();
        passedObject.put("generalResponse", generalResponse);
        return new ModelAndView("report_leave_balance").addAllObjects(passedObject);
    }

    @RequestMapping(value = "/report_leave_schedule")
    public ModelAndView reportingSchedule(HttpServletRequest httpServletRequest, String leaveName, String startDate, String endDate) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        GeneralResponse generalResponse = reportingService.findAllScheduleByParams(leaveName,startDate,endDate);
        HashMap<String, GeneralResponse> passedObject = getQueryList();
        passedObject.put("generalResponse", generalResponse);
        return new ModelAndView("report_leave_schedule").addAllObjects(passedObject);
    }

    @RequestMapping(value = "/history_leave_request")
    public ModelAndView historyLeaveRequest(HttpServletRequest httpServletRequest, String employeeId, String employeeName, String leaveName,
                                            String createdDate, String dateFrom, String dateTo, String projectCode, String projectName,
                                            String clientName, String clientCode, String groupCompany) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        GeneralResponse generalResponse = leaveRequestService.getAllLeaveRequestByParams("Cancelled",employeeId,employeeName,leaveName,createdDate,dateFrom,dateTo,projectCode,projectName,clientName,clientCode,groupCompany);
        HashMap<String, GeneralResponse> passedObject = getQueryList();
        passedObject.put("generalResponse", generalResponse);
        return new ModelAndView("history_leave_request").addAllObjects(passedObject);
    }

    @RequestMapping(value = "/history_leave_balance")
    public ModelAndView historyLeaveBalance(HttpServletRequest httpServletRequest, String employeeId, String employeeName, String leaveName,
                                            String expiredDate, String createdDate, String projectCode, String projectName,
                                            String clientName, String clientCode, String groupCompany) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        GeneralResponse generalResponse = removedLeaveBalanceService.getAllRemovedLeaveBalanceByParams(employeeId,employeeName,leaveName,expiredDate,createdDate,projectCode,projectName,clientName,clientCode,groupCompany);
        HashMap<String, GeneralResponse> passedObject = getQueryList();
        passedObject.put("generalResponse", generalResponse);
        return new ModelAndView("history_leave_balance").addAllObjects(passedObject);
    }

    @RequestMapping(value = "/admin_create")
    public ModelAndView adminCreate(HttpServletRequest httpServletRequest) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        return new ModelAndView("admin_create").addObject(userService.getAllUser());
    }

    @RequestMapping(value = "/serverTime")
    @ResponseBody
    public GeneralResponse serverTime(HttpServletRequest httpServletRequest) {
        return null;
    }

    @RequestMapping(value = "/leavetype_manage_edit")
    public ModelAndView leaveTypeEdit(Long id, HttpServletRequest httpServletRequest) {
        if (!userService.isUserValid(httpServletRequest)) {
            return new ModelAndView("login");
        }
        GeneralResponse generalResponse = leaveTypeService.getLeaveTypeById(id);
        return new ModelAndView("leavetype_manage_edit").addObject("generalResponse", generalResponse);
    }

    private HashMap<String,GeneralResponse> getQueryList(){
        HashMap<String, GeneralResponse> passedObject = new HashMap<>();
        GeneralResponse projectList = projectService.getAllProject();
        GeneralResponse clientList = clientSevice.getAllClient();
        GeneralResponse groupCompanyList = projectService.getAllGroupCompany();
        GeneralResponse leaveTypeList = leaveTypeService.getAllLeaveType();
        passedObject.put("clientList", clientList);
        passedObject.put("projectList", projectList);
        passedObject.put("groupCompanyList", groupCompanyList);
        passedObject.put("leaveTypeList", leaveTypeList);

        return passedObject;
    }
}
