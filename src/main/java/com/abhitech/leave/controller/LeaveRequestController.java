package com.abhitech.leave.controller;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.bean.LeaveRequestCancelBean;
import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.LeaveRequest;
import com.abhitech.leave.service.LeaveRequestService;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LeaveRequestController {

    @Autowired
    LeaveRequestService leaveRequestService;

    @RequestMapping(value = "/getAllLeaveRequest")
    @ResponseBody
    public GeneralResponse getAllLeaveRequest(HttpServletRequest httpServletRequest) {
        return leaveRequestService.getAllLeaveRequest();
    }

//    @RequestMapping(value = "/updateLeaveRequest")
//    @ResponseBody
//    public GeneralResponse updateLeaveRequest(HttpServletRequest httpServletRequest, @ModelAttribute LeaveRequest leaveRequest) {
//        return leaveRequestService.updateLeaveRequest(leaveRequest);
//    }

    @RequestMapping(value = "/getLeaveRequestById")
    @ResponseBody
    public GeneralResponse getLeaveRequestById(HttpServletRequest httpServletRequest, long id) {
        return leaveRequestService.getLeaveRequestById(id);
    }

    @RequestMapping(value = "/getLeaveRequestByEmployee")
    @ResponseBody
    public GeneralResponse getLeaveRequestById(HttpServletRequest httpServletRequest, Employee employee) {
        return leaveRequestService.getLeaveRequestByEmployee(employee);
    }

    @RequestMapping(value = "/createLeaveRequestUnsafe")
    @ResponseBody
    public GeneralResponse createLeaveRequestUnsafe(LeaveRequest leaveRequest) {
        try {
            return leaveRequestService.createLeaveRequestUnsafe(leaveRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return GeneralResponseUtil.resultFailureWithException(e);
        }
    }

    @RequestMapping(value = "/createLeaveRequest")
    @ResponseBody
    public GeneralResponse createLeaveRequest(HttpServletRequest httpServletRequest, LeaveRequest leaveRequest) {
        try {
            return leaveRequestService.createLeaveRequest(httpServletRequest, leaveRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return GeneralResponseUtil.resultFailureWithException(e);
        }
    }

    @RequestMapping(value = "/cancelLeaveRequest")
    @ResponseBody
    public GeneralResponse cancelLeaveRequest(HttpServletRequest httpServletRequest, LeaveRequestCancelBean leaveRequestCancelBean) {
        try {
            return leaveRequestService.cancelLeaveRequest(leaveRequestCancelBean);
        } catch (Exception e) {
            return GeneralResponseUtil.resultFailureWithException(e);
        }
    }

}
