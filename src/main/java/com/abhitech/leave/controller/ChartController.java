package com.abhitech.leave.controller;

import com.abhitech.leave.bean.ChartBean;
import com.abhitech.leave.service.ChartService;
import com.abhitech.leave.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ChartController {

    @Autowired
    DashboardService dashboardService;
    @Autowired
    ChartService chartService;

    @RequestMapping(value = "/updateChart")
    public ChartBean updateChart(HttpServletRequest httpServletRequest, String dateFrom, String dateTo, String employeeId, String departmentName, String projectCode) throws Exception {
        return chartService.generateLeaveRequestChart(dateFrom, dateTo, employeeId == null || employeeId.trim().isEmpty() ? null : employeeId, departmentName == null || departmentName.trim().isEmpty() ? null : departmentName, projectCode == null || projectCode.trim().isEmpty() ? null : projectCode);
    }

    @RequestMapping(value = "/getStartingChart")
    public ChartBean starterChart(HttpServletRequest httpServletRequest) throws Exception {
        return chartService.generateLeaveRequestChart10Days();
    }
}
