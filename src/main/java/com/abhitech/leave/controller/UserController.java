package com.abhitech.leave.controller;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.service.UserService;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/createAdmin")
    @ResponseBody
    public GeneralResponse login(HttpServletRequest httpServletRequest, String username, String password) throws Exception {
        return GeneralResponseUtil.resultSuccessWithData(userService.createUser(username, password));
    }

    @RequestMapping(value = "/disableAdmin")
    @ResponseBody
    public GeneralResponse disableAdmin(HttpServletRequest httpServletRequest, long id) throws Exception {
        return GeneralResponseUtil.resultSuccessWithData(userService.disableUser(id));
    }


}
