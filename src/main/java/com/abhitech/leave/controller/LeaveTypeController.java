package com.abhitech.leave.controller;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.model.LeaveType;
import com.abhitech.leave.service.LeaveTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LeaveTypeController {

    @Autowired
    LeaveTypeService leaveTypeService;

    @RequestMapping(value = "/getAllLeaveType")
    @ResponseBody
    public GeneralResponse getAllLeaveType(HttpServletRequest httpServletRequest) {
        return leaveTypeService.getAllLeaveType();
    }

    @RequestMapping(value = "/createLeaveType")
    @ResponseBody
    public GeneralResponse createLeaveType(HttpServletRequest httpServletRequest, LeaveType leaveType) {
        return leaveTypeService.createLeaveType(httpServletRequest, leaveType);
    }

    @RequestMapping(value = "/updateLeaveType")
    @ResponseBody
    public GeneralResponse updateLeaveType(HttpServletRequest httpServletRequest, LeaveType leaveType) {
        return leaveTypeService.updateLeaveType(leaveType);
    }

    @RequestMapping(value = "/getLeaveTypeById")
    @ResponseBody
    public GeneralResponse getLeaveTypeById(HttpServletRequest httpServletRequest, long id) {
        return leaveTypeService.getLeaveTypeById(id);
    }


    /*@RequestMapping(value = "/testObject")
    public @ResponseBody GeneralResponse testObject(long id, HttpServletRequest httpServletRequest) {
        GeneralResponse generalResponse = leaveTypeService.getLeaveTypeById(id);
        return generalResponse;
    }*/

}
