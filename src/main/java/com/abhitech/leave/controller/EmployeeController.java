package com.abhitech.leave.controller;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.service.EmployeeService;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


@Controller
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping(value = "/getEmployeeBalance")
    public String getEmployeeBalance(long id, HttpServletRequest httpServletRequest) {
        httpServletRequest.getSession().setAttribute("generalResponse", employeeService.getEmployeeLeaveBalanceById(id));
        return "employee_fragments :: employeeBalanceTable";
    }

    @GetMapping(value = "/getEmployeeBalanceForRequestForm")
    public String getEmployeeBalanceNoEdit(long id, HttpServletRequest httpServletRequest) {
        httpServletRequest.getSession().setAttribute("generalResponse", employeeService.getEmployeeLeaveBalanceById(id));
        return "leaverequest_fragments :: employeeBalanceTable";
    }

    @GetMapping(value = "/getBalanceById")
    public String getBalanceById(long id, HttpServletRequest httpServletRequest) {
        httpServletRequest.getSession().setAttribute("generalResponse", employeeService.getEmployeeLeaveBalanceById(id));
        return "employee_fragments :: formUpdateBalance";
    }

    @RequestMapping(value = "/getEmployeeById")
    @ResponseBody
    public GeneralResponse getEmployeeById(HttpServletRequest httpServletRequest, Long id) {
        return employeeService.getEmployeeById(id);
    }

    @RequestMapping(value = "/updateEmployee")
    @ResponseBody
    public GeneralResponse updateEmployee(String employeeId, String enrolledDate, String leaveActivationDate, String contractDate, String contractEndDate, String probationEndDate) {
        try {
            return employeeService.setEmployeeLeaveActivationDateById(employeeId, enrolledDate, leaveActivationDate, contractDate, contractEndDate, probationEndDate);
        } catch (Exception e) {
            e.printStackTrace();
            return GeneralResponseUtil.resultFailureWithException(e);
        }
    }

    /*@RequestMapping(value = "/getEmployeeLeavesTypeById")
    @ResponseBody
    public GeneralResponse getEmployeeLeaveTypeById(HttpServletRequest httpServletRequest, Long id) {
        return employeeService.getEmployeeLeaveTypesById(id);
    }*/
/*

    @GetMapping(value = "/getLeaveTypeDropdownById")
    public String getLeaveTypeDropdown(long id, HttpServletRequest httpServletRequest) {
        httpServletRequest.getSession().setAttribute("generalResponse", employeeService.getEmployeeLeaveTypesById(id));
        return "leaverequest_fragments :: leaveTypeDropdown";
    }
*/
/*
    @RequestMapping(value = "/testObject")
    public @ResponseBody
    GeneralResponse testObject(long id, HttpServletRequest httpServletRequest) {
        GeneralResponse generalResponse = employeeService.getEmployeeLeaveTypesById(id);
        return generalResponse;
    }*/
}
