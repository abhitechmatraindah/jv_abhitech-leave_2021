package com.abhitech.leave.controller;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.LeaveBalance;
import com.abhitech.leave.service.LeaveBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "/leaveBalance")
public class LeaveBalanceController {

    @Autowired
    LeaveBalanceService leaveBalanceService;

    @RequestMapping(value = "/addBalance")
    @ResponseBody
    public GeneralResponse addBalance(HttpServletRequest httpServletRequest, LeaveBalance leaveBalance, int amount) {
        return leaveBalanceService.addBalance(httpServletRequest, leaveBalance, amount);
    }

    @RequestMapping(value = "/getLeaveBalance")
    @ResponseBody
    public GeneralResponse getLeaveBalance(HttpServletRequest httpServletRequest, Employee employee) {
        return leaveBalanceService.getListLeaveBalanceByEmployee(employee);
    }

    @RequestMapping(value = "/getLeaveBalanceUnsecured")
    @ResponseBody
    public GeneralResponse getLeaveBalanceUnsecured(HttpServletRequest httpServletRequest, String employeeId) {
        return leaveBalanceService.getListLeaveBalanceByEmployeeUnsecured(employeeId);
    }

    @RequestMapping(value = "/deductBalance")
    @ResponseBody
    public GeneralResponse deductBalance(HttpServletRequest httpServletRequest, LeaveBalance leaveBalance, int amount) {
        return leaveBalanceService.deductBalance(httpServletRequest, leaveBalance, amount);
    }
}
