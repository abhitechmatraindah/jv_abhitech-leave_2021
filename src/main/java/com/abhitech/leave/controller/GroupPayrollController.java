package com.abhitech.leave.controller;

import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.service.GroupPayrollService;
import com.abhitech.leave.service.LeaveTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class GroupPayrollController {
    @Autowired
    GroupPayrollService groupPayrollService;
    @Autowired
    LeaveTypeService leaveTypeService;

    @RequestMapping(value = "/getGroupPayrollById")
    @ResponseBody
    public GeneralResponse getGroupPayrollById(HttpServletRequest httpServletRequest, Long id) {
        return groupPayrollService.getGroupPayrollById(id);
    }

    @RequestMapping(value = "/getGroupPayrollByProject")
    @ResponseBody
    public GeneralResponse getGroupPayrollByProject(HttpServletRequest httpServletRequest, Long projectId) {
        return groupPayrollService.getGroupPayrollByProjectId(projectId);
    }


    @RequestMapping(value = "/updateGroupPayroll")
    @ResponseBody
    public GeneralResponse updateGroupPayrollDefaultLeaveType(HttpServletRequest httpServletRequest, Long id, Long defaultLeaveType) {
        return groupPayrollService.updateGroupPayroll(httpServletRequest, id, defaultLeaveType);
    }

    @RequestMapping(value = "/updateGroupPayrollLeaveType")
    @ResponseBody
    public GeneralResponse updateGroupPayrollLeaveType(HttpServletRequest httpServletRequest, Long id, String[] leaveTypeIds) {
        return groupPayrollService.updateGroupPayrollLeaveType(id, leaveTypeIds);
    }

    @RequestMapping(value = "/getLeaveTypesByGroupPayrollId")
    public GeneralResponse getLeaveTypesByGroupPayrollId(Long id, HttpServletRequest httpServletRequest) {
        return groupPayrollService.getGroupPayrollActiveLeaveTypes(id);
    }

    @RequestMapping(value = "/getLeaveTypesTable")
    public String getLeaveTypesTable(Long id, HttpServletRequest httpServletRequest) {
        httpServletRequest.getSession().setAttribute("currentLeaveTypes", groupPayrollService.getGroupPayrollActiveLeaveTypes(id));
        return "groupPayroll_fragments :: leaveTypesTable";
    }

    @RequestMapping(value = "/getPastLeaveTypesTable")
    public String getPastLeaveTypesTable(Long id, HttpServletRequest httpServletRequest) {
        httpServletRequest.getSession().setAttribute("pastLeaveTypes", groupPayrollService.getGroupPayrollInactiveLeaveTypes(id));
        return "groupPayroll_fragments :: pastLeaveTypesTable";
    }

    @RequestMapping(value = "/getBindableLeaveTypes")
    public String getBindableLeaveTypes(Long id, HttpServletRequest httpServletRequest) {
        httpServletRequest.getSession().setAttribute("bindableLeaveTypes", groupPayrollService.getAllLeaveTypeEnabledNoDefaultLeaveType(id));
        return "groupPayroll_fragments :: bindableLeaveTypes";
    }

}
