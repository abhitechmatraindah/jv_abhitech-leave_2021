package com.abhitech.leave.controller;

import com.abhitech.leave.bean.EmployeeResponse;
import com.abhitech.leave.bean.GeneralResponse;
import com.abhitech.leave.bean.LeaveRequestCancelBean;
import com.abhitech.leave.model.LeaveRequest;
import com.abhitech.leave.service.*;
import com.abhitech.leave.util.GeneralResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "/secured")
public class ApiSecuredController {

    @Autowired
    ApiService apiService;

    @Autowired
    ProjectService projectService;

    @Autowired
    LeaveBalanceService leaveBalanceService;

    @Autowired
    LeaveRequestService leaveRequestService;

    @Autowired
    GroupPayrollService groupPayrollService;

    @RequestMapping(value = {"/uploadLeaveBalances"}, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public GeneralResponse uploadLeaveBalances(HttpServletRequest httpServletRequest, MultipartFile multipartFile) {
        return leaveBalanceService.updateLeaveBalanceFromFile(multipartFile);
    }

    @RequestMapping(value = {"/getAllProject"})
    @ResponseBody
    public GeneralResponse getAllProject(HttpServletRequest httpServletRequest) {
        return projectService.getAllProject();
    }

//    @RequestMapping(value = {"/getGroupPayrollByProjectId"})
//    @ResponseBody
//    public GeneralResponse getGroupPayrollByProjectId(HttpServletRequest httpServletRequest, @RequestParam String id) {
//        GroupPayrollResponse groupPayrollResponse = apiService.getGroupPayrollByProjectId(id);
//        if (groupPayrollResponse != null)
//            return GeneralResponseUtil.resultSuccessWithData(groupPayrollResponse.getResult());
//        return GeneralResponseUtil.resultFailureNoData();
//    }

    @RequestMapping(value = {"/getEmployeeByGroupPayrollId"})
    @ResponseBody
    public GeneralResponse getEmployeeByGroupPayrollId(HttpServletRequest httpServletRequest, @RequestParam String id) {
        EmployeeResponse employeeResponse = apiService.getEmployeeByGroupPayrollId(id);
        if (employeeResponse != null)
            return GeneralResponseUtil.resultSuccessWithData(employeeResponse.getResult());
        return GeneralResponseUtil.resultFailureNoData();
    }

    @RequestMapping(value = "/getLeaveBalance")
    @ResponseBody
    public GeneralResponse getLeaveBalanceUnsecured(String employeeId) {
        return leaveBalanceService.getListLeaveBalanceByEmployeeUnsecured(employeeId);
    }

    @RequestMapping(value = "/getEmployeeDetail")
    @ResponseBody
    public GeneralResponse getEmployeeDetail(String employeeId) {
        return leaveBalanceService.getEmployeeDetail(employeeId);
    }

    @RequestMapping(value = "/createLeaveRequest")
    @ResponseBody
    public GeneralResponse createLeaveRequestUnsafe(LeaveRequest leaveRequest) {
        try {
            return leaveRequestService.createLeaveRequestUnsafe(leaveRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return GeneralResponseUtil.resultFailureWithException(e);
        }
    }

    @RequestMapping(value = "/cancelLeaveRequest")
    @ResponseBody
    public GeneralResponse cancelLeaveRequest(LeaveRequestCancelBean leaveRequestCancelBean) {
        try {
            return leaveRequestService.cancelLeaveRequest(leaveRequestCancelBean);
        } catch (Exception e) {
            return GeneralResponseUtil.resultFailureWithException(e);
        }
    }

    @RequestMapping(value = "/getLeaveRequestByEmployeeId")
    @ResponseBody
    public GeneralResponse getLeaveRequestById(String employeeId) {
        return leaveRequestService.getLeaveRequestByEmployeeId(employeeId);
    }

    @RequestMapping(value = "/getLeaveTypesByGroupPayrollId")
    public GeneralResponse getLeaveTypesByGroupPayrollId(Long groupPayrollId) {
        return groupPayrollService.getGroupPayrollActiveLeaveTypes(groupPayrollId);
    }
}
