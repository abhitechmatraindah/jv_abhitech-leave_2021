package com.abhitech.leave.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

/**
 * Configuration for swagger
 */
//@Configuration
//@EnableSwagger2
//public class SwaggerConfig {
//
////    @Value("${swagger.host-mapping}")
////    private String HOST_MAPPING;
//
////    @Value("${swagger.path-mapping}")
////    private String PATH_MAPPING;
//
//    /**
//     * Configuration swagger with Authorization header
//     * if prefix url needed, set value at ${swagger.path-mapping}
//     *
//     * @return docker api
//     */
//    @Bean
//    public Docket api() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .select()
//                .apis(RequestHandlerSelectors.any())
//                .paths(PathSelectors.any())
//                .build()
////                .host(HOST_MAPPING)
////                .pathMapping(PATH_MAPPING)
//                .securitySchemes(getSecuritySchemes())
//                .securityContexts(getSecurityContexts());
//    }
//
//    private List<ApiKey> getSecuritySchemes() {
//        return Collections.singletonList(new ApiKey("JWT", "Authorization", "header"));
//    }
//
//    private List<SecurityContext> getSecurityContexts() {
//        return Collections.singletonList(
//                SecurityContext.builder().securityReferences(
//                        Collections.singletonList(SecurityReference.builder()
//                                .reference("JWT")
//                                .scopes(new AuthorizationScope[0])
//                                .build()
//                        )
//                ).build()
//        );
//    }
//}
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(Collections.singletonList(new ApiKey("JWT", "Authorization", "header")))
                .securityContexts(Collections.singletonList(SecurityContext.builder()
                        .securityReferences(Collections
                                .singletonList(SecurityReference
                                        .builder()
                                        .reference("JWT")
                                        .scopes(new AuthorizationScope[0])
                                        .build()))
                        .build()));
    }
}
