package com.abhitech.leave.repository;

import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.RemovedLeaveBalance;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RemovedLeaveBalanceRepo extends CrudRepository<RemovedLeaveBalance, Long> {

    @Query(value = "SELECT rlb.* FROM removed_leave_balance rlb JOIN leave_type ON leave_type.id = rlb.leave_type_id JOIN employee ON employee.id = rlb.employee_id JOIN group_payroll ON group_payroll.id = employee.group_payroll_id JOIN project ON group_payroll.project_id=project.id JOIN client ON project.client_id = client.id"
            + " WHERE rlb.expired_date LIKE CONCAT('%', ?4 ,'%') "
            + " AND rlb.created_date LIKE CONCAT('%', ?5 ,'%') "
            + " AND employee.employee_id LIKE CONCAT('%', ?1 ,'%') "
            + " AND employee.employee_name LIKE CONCAT('%', ?2 ,'%') "
            + " AND leave_type.leave_name LIKE CONCAT('%', ?3 ,'%') "
            + " AND project.project_code LIKE CONCAT('%', ?6 ,'%') "
            + " AND project.project_name LIKE CONCAT('%', ?7 ,'%') "
            + " AND client.client_name LIKE CONCAT('%', ?8 ,'%') "
            + " AND client.client_code LIKE CONCAT('%', ?9 ,'%') "
            + " AND project.group_company LIKE CONCAT('%', ?10 ,'%')", nativeQuery = true)
    Iterable<RemovedLeaveBalance> findAllRemovedLeaveBalanceByParams(String employeeId, String employeeName, String leaveName,
                                                          String expiredDate, String createdDate, String projectCode, String projectName,
                                                                     String clientName, String clientCode, String groupCompany);

}
