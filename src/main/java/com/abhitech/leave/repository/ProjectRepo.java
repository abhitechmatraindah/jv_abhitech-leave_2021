package com.abhitech.leave.repository;

import com.abhitech.leave.model.Project;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProjectRepo extends CrudRepository<Project, Long> {
    Project findByProjectCode(String projectCode);
    int countAllByEnable(boolean enable);
    @Query(value = "SELECT DISTINCT p.group_company FROM project p", nativeQuery = true)
    Iterable<String> findAllGroupCompanyDistinct();
}
