package com.abhitech.leave.repository;

import com.abhitech.leave.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepo extends CrudRepository<User, Long> {
    User findByUsernameAndPasswordEquals(String username, String password);
}
