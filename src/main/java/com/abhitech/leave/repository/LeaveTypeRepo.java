package com.abhitech.leave.repository;

import com.abhitech.leave.model.LeaveType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface LeaveTypeRepo extends CrudRepository<LeaveType, Long> {
    LeaveType findByLeaveCodeAndIsActive(String leaveCode, boolean active);

    List<LeaveType> findByIsActive(Boolean status);

    List<LeaveType> findByExpiredDateBefore(@Temporal(TemporalType.DATE) Date date);

    @Query(value = "select lt.* from leave_type lt WHERE lt.id LIKE CONCAT('%', ?1 ,'%') AND lt.leave_name LIKE CONCAT('%', ?2 ,'%') ", nativeQuery = true)
    Optional<LeaveType> findByIdAndLeaveName(long id, String leaveName);
}
