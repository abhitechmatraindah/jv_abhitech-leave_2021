package com.abhitech.leave.repository;

import com.abhitech.leave.model.Holiday;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface HolidayRepo extends CrudRepository<Holiday, Long> {
    Holiday findBySecureId(String secureId); //findTopByUpdatedBetweenOrderByUpdatedDesc
    List<Holiday> findByHolidayBetween(Date startDate, Date endDate);
}
