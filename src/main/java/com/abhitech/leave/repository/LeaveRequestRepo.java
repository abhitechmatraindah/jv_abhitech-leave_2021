package com.abhitech.leave.repository;

import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.LeaveBalance;
import com.abhitech.leave.model.LeaveRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface LeaveRequestRepo extends CrudRepository<LeaveRequest, Long> {
    LeaveRequest findByLeaveBalanceAndStatus(LeaveBalance leaveBalance, String status);
    @Query(value = "SELECT lr.* from leave_request lr WHERE date_from LIKE CONCAT('%', ?1 ,'%') AND date_to LIKE CONCAT('%', ?2 ,'%')", nativeQuery = true)
    Iterable<LeaveRequest> findAllByDateFromAndDateTo(String startDate, String endDate);

    void deleteByStatus(String status);

    @Query(value = "SELECT sum(amount) FROM leave_request WHERE created_date between ?1 and ?2", nativeQuery = true)
    Long sumLeaveRequestByDate(LocalDate dateFrom, LocalDate dateTo);

    @Query(value = "SELECT COUNT(DISTINCT employee_id) FROM leave_request WHERE date_from <= ?1 AND date_to >= ?1", nativeQuery = true)
    Long countDistinctByEmployeeAndTodayDate(LocalDate todayDate);

    @Query(value = "SELECT sum(amount) FROM leave_request WHERE date_from <= ?1 AND date_to >= ?1 AND status = 'SUBMITTED' AND leave_type_id = ?2", nativeQuery = true)
    Integer sumLeaveAmountByDate(LocalDate date, Long leaveTypeId);

    List<LeaveRequest> findByDateToBefore(Date date);

    List<LeaveRequest> findByEmployee(Employee employee);

    List<LeaveRequest> findByEmployeeId(Long employeeId);

    List<LeaveRequest> findAllByStatusOrderByCreatedDateDesc(String status);

    @Query(value = "SELECT sum(amount) FROM leave_request JOIN employee ON leave_request.employee_id = employee.id JOIN project ON employee.project=project.project_name "
            + " WHERE date_from <= ?1 AND date_to >= ?1 AND status = 'SUBMITTED' AND leave_type_id = ?2 "
            + "AND (?3 is null or employee.employee_id = ?3) AND (?4 is null or employee.department_name LIKE CONCAT('%', ?4 ,'%')) "
            + " AND (?5 is null or project.project_code = ?5)", nativeQuery = true)
    Integer sumLeaveAmountByParams(LocalDate date, Long leaveTypeId, String employeeId, String departmentName, String projectCode);

    @Query(value = "SELECT sum(amount) FROM leave_request WHERE leave_balance_id = ?1", nativeQuery = true)
    Integer sumLeaveAmountByLeaveBalanceId(Long leaveBalanceId);

    @Query(value = "SELECT lr.* FROM leave_request lr JOIN leave_type ON leave_type.id = lr.leave_type_id JOIN employee ON employee.id = lr.employee_id " +
            "JOIN group_payroll ON group_payroll.id = employee.group_payroll_id JOIN project ON group_payroll.project_id=project.id " +
            "JOIN client ON project.client_id = client.id"
            + " WHERE employee.employee_id LIKE CONCAT('%', ?1 ,'%') "
            + " AND employee.employee_name LIKE CONCAT('%', ?2 ,'%') "
            + " AND leave_type.leave_name LIKE CONCAT('%', ?3 ,'%') "
            + " AND lr.created_date LIKE CONCAT('%', ?4 ,'%') "
            + " AND lr.date_from LIKE CONCAT('%', ?5 ,'%') "
            + " AND lr.date_to LIKE CONCAT('%', ?6 ,'%') "
            + " AND project.project_code LIKE CONCAT('%', ?7 ,'%') "
            + " AND project.project_name LIKE CONCAT('%', ?8 ,'%') "
            + " AND client.client_name LIKE CONCAT('%', ?9 ,'%') "
            + " AND client.client_code LIKE CONCAT('%', ?10 ,'%') "
            + " AND project.group_company LIKE CONCAT('%', ?11 ,'%')", nativeQuery = true)
    Iterable<LeaveRequest> findAllLeaveRequestByParamsAll(String employeeId, String employeeName, String leaveName,
                                                                String createdDate, String dateFrom, String dateTo, String projectCode, String projectName,
                                                                String clientName, String clientCode, String groupCompany);

    @Query(value = "SELECT lr.* FROM leave_request lr JOIN leave_type ON leave_type.id = lr.leave_type_id JOIN employee ON employee.id = lr.employee_id " +
            "JOIN group_payroll ON group_payroll.id = employee.group_payroll_id JOIN project ON group_payroll.project_id=project.id " +
            "JOIN client ON project.client_id = client.id"
            + " WHERE employee.employee_id LIKE CONCAT('%', ?1 ,'%') "
            + " AND employee.employee_name LIKE CONCAT('%', ?2 ,'%') "
            + " AND leave_type.leave_name LIKE CONCAT('%', ?3 ,'%') "
            + " AND lr.created_date LIKE CONCAT('%', ?4 ,'%') "
            + " AND lr.date_from LIKE CONCAT('%', ?5 ,'%') "
            + " AND lr.date_to LIKE CONCAT('%', ?6 ,'%') "
            + " AND project.project_code LIKE CONCAT('%', ?7 ,'%') "
            + " AND project.project_name LIKE CONCAT('%', ?8 ,'%') "
            + " AND client.client_name LIKE CONCAT('%', ?9 ,'%') "
            + " AND client.client_code LIKE CONCAT('%', ?10 ,'%') "
            + " AND project.group_company LIKE CONCAT('%', ?11 ,'%')"
            + " AND lr.status = 'ENDED' OR 'CANCELLED' ORDER BY lr.created_date", nativeQuery = true)
    Iterable<LeaveRequest> findAllLeaveRequestByParamsCancelled(String employeeId, String employeeName, String leaveName,
                                                          String createdDate, String dateFrom, String dateTo, String projectCode, String projectName,
                                                          String clientName, String clientCode, String groupCompany);

}
