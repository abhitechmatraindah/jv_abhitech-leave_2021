package com.abhitech.leave.repository;

import com.abhitech.leave.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepo extends CrudRepository<Client, Long> {
    Client findByClientCode(String clientCode);
}
