package com.abhitech.leave.repository;

import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.GroupPayroll;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepo extends CrudRepository<Employee, Long> {
    Employee findBySecureId(String secureId);

    Employee getByEmployeeId(String employeeId);

    List<Employee> findEmployeeByGroupPayroll(GroupPayroll groupPayroll);

    Long countAllByIsActive(boolean isActive);

    Optional<Employee> findByEmployeeIdEquals(String employeeId);

    Employee findByEmployeeIdAndIsActive(String employeeId, boolean isActive);

    @Query(value = "SELECT e.* FROM employee e JOIN group_payroll ON group_payroll.id = e.group_payroll_id JOIN project ON group_payroll.project_id=project.id JOIN client ON project.client_id = client.id"
            + " WHERE e.employee_id LIKE CONCAT('%', ?1 ,'%') "
            + " AND e.employee_name LIKE CONCAT('%', ?2 ,'%') "
            + " AND project.project_code LIKE CONCAT('%', ?3 ,'%') "
            + " AND project.project_name LIKE CONCAT('%', ?4 ,'%') "
            + " AND client.client_name LIKE CONCAT('%', ?5 ,'%') "
            + " AND client.client_code LIKE CONCAT('%', ?6 ,'%') "
            + " AND project.group_company LIKE CONCAT('%', ?7 ,'%') ORDER BY e.employee_name", nativeQuery = true)
    Iterable<Employee> findAllEmployeeByParams(String employeeId, String employeeName, String projectCode, String projectName, String clientName, String clientCode, String groupCompany);

}
