package com.abhitech.leave.repository;

import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.LeaveBalance;
import com.abhitech.leave.model.LeaveType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

public interface LeaveBalanceRepo extends CrudRepository<LeaveBalance, Long> {
    List<LeaveBalance> findByStatusAndExpired(boolean status, boolean expired);

    List<LeaveBalance> findByEmployee(Employee employee);

    List<LeaveBalance> findByEmployeeAndStatusAndExpired(Employee employee, boolean status, boolean expired);

    List<LeaveBalance> findByEmployeeAndLeaveType(Employee employee, LeaveType leaveType);

    List<LeaveBalance> findByNextAddDateBefore(@Temporal(TemporalType.DATE) Date date);

    List<LeaveBalance> findByNextRestartDateBefore(@Temporal(TemporalType.DATE) Date date);

    List<LeaveBalance> findByExpiredDateBefore(@Temporal(TemporalType.DATE) Date date);

    int countAllByStatus(boolean status);

    @Query(value = "SELECT count(DISTINCT employee_id) FROM leave_balance WHERE balance < 0", nativeQuery = true)
    Long countDistinctEmployeeByBalanceNegative();

    @Query(value = "SELECT sum(balance) FROM leave_balance WHERE status = true", nativeQuery = true)
    Long sumBalanceFromLeaveBalanceActive();
}
