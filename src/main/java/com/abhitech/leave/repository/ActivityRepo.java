package com.abhitech.leave.repository;

import com.abhitech.leave.model.Activity;
import com.abhitech.leave.model.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ActivityRepo extends CrudRepository<Activity, Long> {
    @Query(value = "SELECT a.* FROM activity a"
            + " WHERE a.created_date LIKE CONCAT('%', ?1 ,'%')"
            + " AND a.last_update_date LIKE CONCAT('%', ?2 ,'%')"
            + " AND a.from_user_id LIKE CONCAT('%', ?3 ,'%') "
            + " AND a.to_user_id LIKE CONCAT('%', ?4 ,'%') ORDER BY a.created_date", nativeQuery = true)
    Iterable<Activity> findAllActivityByParams(String createdDate, String lastUpdateDate, String fromUserId, String toUserId);

}
