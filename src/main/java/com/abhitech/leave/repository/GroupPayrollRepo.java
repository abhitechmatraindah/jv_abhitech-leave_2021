package com.abhitech.leave.repository;

import com.abhitech.leave.model.GroupPayroll;
import com.abhitech.leave.model.LeaveType;
import com.abhitech.leave.model.Project;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface GroupPayrollRepo extends CrudRepository<GroupPayroll, Long> {
    GroupPayroll findByGroupCode(String groupCode);
    GroupPayroll findBySecureId(String secureId);

    @Query(value = "select gp.* from group_payroll gp join group_payroll_leave_types gplt ON gplt.group_payroll_id = gp.id where gplt.leave_types_id = 523", nativeQuery = true)
    List<GroupPayroll> getGroupPayrollsByLeaveType(LeaveType leaveType);

    List<GroupPayroll> findByProject(Project project);

    Optional<GroupPayroll> findById(String id);
}
