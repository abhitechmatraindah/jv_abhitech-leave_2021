package com.abhitech.leave.util;

import com.abhitech.leave.model.Employee;
import com.abhitech.leave.model.LeaveBalance;
import com.abhitech.leave.model.LeaveType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class LeaveTypeUtil {
    static Logger logger = LoggerFactory.getLogger(LeaveTypeUtil.class);

    public static Date getExpDateFromEmployeeAndLeaveType(LeaveType leaveType, Employee employee) throws ParseException {
        Date result = null;
        Date employeeDate = null;
        if (leaveType.getExpStartDateType().equalsIgnoreCase("ENROLLEDDATE")) { // ENROLLEDDATE, LEAVEACTIVATIONDATE, PROBATIONENDDATE, CONTRACTDATE
            employeeDate = employee.getDateEnrolled();
        } else if (leaveType.getExpStartDateType().equalsIgnoreCase("LEAVEACTIVATIONDATE")) {
            employeeDate = employee.getLeaveActivationDate();
        } else if (leaveType.getExpStartDateType().equalsIgnoreCase("PROBATIONENDDATE")) {
            employeeDate = employee.getProbationEndDate();
        } else if (leaveType.getExpStartDateType().equalsIgnoreCase("CONTRACTDATE")) {
            employeeDate = employee.getContractStartDate();
        } else if (leaveType.getExpStartDateType().equalsIgnoreCase("SPECIFICDATE")) {
            employeeDate = DateUtil.generalDateFormat.parse(leaveType.getExpStartDateTypeValue());
        }
        if (employeeDate != null) {
            Calendar employeeCalendar = Calendar.getInstance();
            employeeCalendar.setTime(employeeDate);
            if (leaveType.getExpDateType().equalsIgnoreCase("DATE")) { //DATE, DAYS, MONTHS, YEARS, NONE
                result = DateUtil.generalDateFormat.parse(leaveType.getExpDateValue());
            } else if (leaveType.getExpDateType().equalsIgnoreCase("DAYS")) {
                employeeCalendar.add(Calendar.DATE, Integer.parseInt(leaveType.getExpDateValue()));
                result = employeeCalendar.getTime();
            } else if (leaveType.getExpDateType().equalsIgnoreCase("MONTHS")) {
                employeeCalendar.add(Calendar.MONTH, Integer.parseInt(leaveType.getExpDateValue()));
                result = employeeCalendar.getTime();
            } else if (leaveType.getExpDateType().equalsIgnoreCase("YEARS")) {
                employeeCalendar.add(Calendar.YEAR, Integer.parseInt(leaveType.getExpDateValue()));
                result = employeeCalendar.getTime();
            } else if (leaveType.getExpDateType().equalsIgnoreCase("NONE")) {
                result = employeeDate;
            }
        }
        logger.info("EXPIRED DATE: " + result);
        return result;
    }

    public static Date getAddDateFromEmployeeAndLeaveType(LeaveType leaveType, Employee employee) {
        Date result = null;
        Date employeeDate = null;
        try {
            // GET EMPLOYEE DATE BASED ON LEAVE TYPE
            if (leaveType.getAddStartDateType().equalsIgnoreCase("ENROLLEDDATE")) { // ENROLLEDDATE, LEAVEACTIVATIONDATE, PROBATIONENDDATE, CONTRACTDATE, SPECIFICDATE
                employeeDate = employee.getDateEnrolled();
            } else if (leaveType.getAddStartDateType().equalsIgnoreCase("LEAVEACTIVATIONDATE")) {
                employeeDate = employee.getLeaveActivationDate();
            } else if (leaveType.getAddStartDateType().equalsIgnoreCase("PROBATIONENDDATE")) {
                employeeDate = employee.getProbationEndDate();
            } else if (leaveType.getAddStartDateType().equalsIgnoreCase("CONTRACTDATE")) {
                employeeDate = employee.getContractStartDate();
            } else if (leaveType.getAddStartDateType().equalsIgnoreCase("SPECIFICDATE")) {
                employeeDate = DateUtil.generalDateFormat.parse(leaveType.getAddStartDateTypeValue());
            }
            if (employeeDate != null) {
                Calendar employeeCalendar = Calendar.getInstance();
                employeeCalendar.setTime(employeeDate);
                if (leaveType.getAddDateType().equalsIgnoreCase("DATE")) { //DATE, DAYS, MONTHS, YEARS, NONE
                    result = DateUtil.generalDateFormat.parse(leaveType.getAddDateValue());
                } else if (leaveType.getAddDateType().equalsIgnoreCase("DAYS")) {
                    employeeCalendar.add(Calendar.DATE, Integer.parseInt(leaveType.getAddDateValue()));
                    result = employeeCalendar.getTime();
                } else if (leaveType.getAddDateType().equalsIgnoreCase("MONTHS")) {
                    employeeCalendar.add(Calendar.MONTH, Integer.parseInt(leaveType.getAddDateValue()));
                    result = employeeCalendar.getTime();
                } else if (leaveType.getAddDateType().equalsIgnoreCase("YEARS")) {
                    employeeCalendar.add(Calendar.YEAR, Integer.parseInt(leaveType.getAddDateValue()));
                    result = employeeCalendar.getTime();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("ADD DATE: " + result);
        return result;
    }

    public static Date getRestartDateFromEmployeeAndLeaveType(LeaveType leaveType, Employee employee) {
        Date result = null;
        Date employeeDate = null;
        try {
            // GET EMPLOYEE DATE BASED ON LEAVE TYPE
            if (leaveType.getRestartStartDateType().equalsIgnoreCase("ENROLLEDDATE")) { // ENROLLEDDATE, LEAVEACTIVATIONDATE, PROBATIONENDDATE, CONTRACTDATE
                employeeDate = employee.getDateEnrolled();
            } else if (leaveType.getRestartStartDateType().equalsIgnoreCase("LEAVEACTIVATIONDATE")) {
                employeeDate = employee.getLeaveActivationDate();
            } else if (leaveType.getRestartStartDateType().equalsIgnoreCase("PROBATIONENDDATE")) {
                employeeDate = employee.getProbationEndDate();
            } else if (leaveType.getRestartStartDateType().equalsIgnoreCase("CONTRACTDATE")) {
                employeeDate = employee.getContractStartDate();
            } else if (leaveType.getRestartStartDateType().equalsIgnoreCase("SPECIFICDATE")) {
                employeeDate = DateUtil.generalDateFormat.parse(leaveType.getRestartStartDateTypeValue());
            }

            if (employeeDate != null) {
                Calendar employeeCalendar = Calendar.getInstance();
                employeeCalendar.setTime(employeeDate);
                if (leaveType.getRestartDateType().equalsIgnoreCase("DATE")) { //DATE, DAYS, MONTHS, YEARS, NONE
                    result = DateUtil.generalDateFormat.parse(leaveType.getRestartDateValue());
                } else if (leaveType.getRestartDateType().equalsIgnoreCase("DAYS")) {
                    employeeCalendar.add(Calendar.DATE, Integer.parseInt(leaveType.getRestartDateValue()));
                    result = employeeCalendar.getTime();
                } else if (leaveType.getRestartDateType().equalsIgnoreCase("MONTHS")) {
                    employeeCalendar.add(Calendar.MONTH, Integer.parseInt(leaveType.getRestartDateValue()));
                    result = employeeCalendar.getTime();
                } else if (leaveType.getRestartDateType().equalsIgnoreCase("YEARS")) {
                    employeeCalendar.add(Calendar.YEAR, Integer.parseInt(leaveType.getRestartDateValue()));
                    result = employeeCalendar.getTime();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("RESTART DATE: " + result);
        return result;
    }

    public static Date getNextExpDateFromLeaveBalance(LeaveBalance leaveBalance) {
        LeaveType leaveType = leaveBalance.getLeaveType();
        Date result = null;
        Date employeeDate = leaveBalance.getExpiredDate();
        try {
            if (employeeDate != null) {
                Calendar employeeCalendar = Calendar.getInstance();
                employeeCalendar.setTime(employeeDate);
                if (leaveType.getExpDateType().equalsIgnoreCase("DATE")) { //DATE, DAYS, MONTHS, YEARS, NONE
                    result = DateUtil.generalDateFormat.parse(leaveType.getExpDateValue());
                } else if (leaveType.getExpDateType().equalsIgnoreCase("DAYS")) {
                    employeeCalendar.add(Calendar.DATE, Integer.parseInt(leaveType.getExpDateValue()));
                    result = employeeCalendar.getTime();
                } else if (leaveType.getExpDateType().equalsIgnoreCase("MONTHS")) {
                    employeeCalendar.add(Calendar.MONTH, Integer.parseInt(leaveType.getExpDateValue()));
                    result = employeeCalendar.getTime();
                } else if (leaveType.getExpDateType().equalsIgnoreCase("YEARS")) {
                    employeeCalendar.add(Calendar.YEAR, Integer.parseInt(leaveType.getExpDateValue()));
                    result = employeeCalendar.getTime();
                }
            } else {
                logger.info("OLD LEAVE BALANCE EXPIRED DATE IS NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Date getNextAddDateFromLeaveBalance(LeaveBalance leaveBalance) {
        LeaveType leaveType = leaveBalance.getLeaveType();
        Date result = null;
        Date employeeDate = leaveBalance.getNextAddDate();
        try {
            if (employeeDate != null) {
                Calendar employeeCalendar = Calendar.getInstance();
                employeeCalendar.setTime(employeeDate);
                if (leaveType.getAddDateType().equalsIgnoreCase("DATE")) { //DATE, DAYS, MONTHS, YEARS, NONE
                    result = DateUtil.generalDateFormat.parse(leaveType.getAddDateValue());
                } else if (leaveType.getAddDateType().equalsIgnoreCase("DAYS")) {
                    employeeCalendar.add(Calendar.DATE, Integer.parseInt(leaveType.getAddDateValue()));
                    result = employeeCalendar.getTime();
                } else if (leaveType.getAddDateType().equalsIgnoreCase("MONTHS")) {
                    employeeCalendar.add(Calendar.MONTH, Integer.parseInt(leaveType.getAddDateValue()));
                    result = employeeCalendar.getTime();
                } else if (leaveType.getAddDateType().equalsIgnoreCase("YEARS")) {
                    employeeCalendar.add(Calendar.YEAR, Integer.parseInt(leaveType.getAddDateValue()));
                    result = employeeCalendar.getTime();
                }
                logger.info("NEXT ADD DATE FROM: " + employeeDate + " to " + result);
            } else {
                logger.info("OLD LEAVE BALANCE EXPIRED DATE IS NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Date getNextRestartDateFromLeaveBalance(LeaveBalance leaveBalance) {
        LeaveType leaveType = leaveBalance.getLeaveType();
        Date result = null;
        Date employeeDate = leaveBalance.getNextRestartDate(); //2020-02-03
        try {
            if (employeeDate != null) {
                Calendar employeeCalendar = Calendar.getInstance();
                employeeCalendar.setTime(employeeDate);
                if (leaveType.getRestartDateType().equalsIgnoreCase("DATE")) { //DATE, DAYS, MONTHS, YEARS, NONE
                    result = DateUtil.generalDateFormat.parse(leaveType.getRestartDateValue());
                } else if (leaveType.getRestartDateType().equalsIgnoreCase("DAYS")) {
                    employeeCalendar.add(Calendar.DATE, Integer.parseInt(leaveType.getRestartDateValue()));
                    result = employeeCalendar.getTime();
                } else if (leaveType.getRestartDateType().equalsIgnoreCase("MONTHS")) {
                    employeeCalendar.add(Calendar.MONTH, Integer.parseInt(leaveType.getRestartDateValue()));
                    result = employeeCalendar.getTime();
                } else if (leaveType.getRestartDateType().equalsIgnoreCase("YEARS")) {
                    employeeCalendar.add(Calendar.YEAR, Integer.parseInt(leaveType.getRestartDateValue()));
                    result = employeeCalendar.getTime();
                }
                logger.info("NEXT RESTART DATE FROM: " + employeeDate + " to " + result);
            } else {
                logger.info("OLD LEAVE BALANCE EXPIRED DATE IS NULL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
