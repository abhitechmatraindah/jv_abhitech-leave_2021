package com.abhitech.leave.util;

import com.abhitech.leave.bean.GeneralResponse;

public class GeneralResponseUtil {

    public static GeneralResponse resultSuccess() {
        return new GeneralResponse("000", "Success", null);
    }

    public static GeneralResponse resultSuccessWithData(Object object) {
        return new GeneralResponse("001", "Success, with data", object);
    }

    public static GeneralResponse resultFailureWithException(Exception e) {
        e.printStackTrace();
        return new GeneralResponse("002", "Failure, " + e.getMessage(), null);
    }

    public static GeneralResponse resultFailureNoData() {
        return new GeneralResponse("002", "Failure, no data", null);
    }

}
