package com.abhitech.leave.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class GroupPayroll {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Date createdDate = new Date();
    private String secureId;
    private String groupCode;
    private String groupName;
    @ManyToOne
    private Project project;
    private Boolean enabled;
    @ManyToMany(fetch = FetchType.EAGER)
    private List<LeaveType> leaveTypes;
    @ManyToOne
    private LeaveType defaultLeaveType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getSecureId() {
        return secureId;
    }

    public void setSecureId(String secureId) {
        this.secureId = secureId;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<LeaveType> getLeaveTypes() {
        return leaveTypes;
    }

    public void setLeaveTypes(List<LeaveType> leaveTypes) {
        this.leaveTypes = leaveTypes;
    }

    public LeaveType getDefaultLeaveType() {
        return defaultLeaveType;
    }

    public void setDefaultLeaveType(LeaveType defaultLeaveType) {
        this.defaultLeaveType = defaultLeaveType;
    }
}
