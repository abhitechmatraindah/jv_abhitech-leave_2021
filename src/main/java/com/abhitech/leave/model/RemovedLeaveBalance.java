package com.abhitech.leave.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class RemovedLeaveBalance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long leaveBalanceId;
    @Temporal(TemporalType.DATE)
    private Date createdDate = new Date();
    @ManyToOne(fetch = FetchType.EAGER)
    private GroupPayroll groupPayroll;
    @ManyToOne(fetch = FetchType.EAGER)
    private Employee employee;
    @ManyToOne(fetch = FetchType.EAGER)
    private LeaveType leaveType;
    private int balance = 0;
    @Temporal(TemporalType.DATE)
    private Date expiredDate;
    @Temporal(TemporalType.DATE)
    private Date lastUpdatedDate = new Date();
    @Temporal(TemporalType.DATE)
    private Date nextAddDate;
    @Temporal(TemporalType.DATE)
    private Date nextRestartDate;
    @Temporal(TemporalType.DATE)
    private Date lastAddDate;
    @Temporal(TemporalType.DATE)
    private Date lastRestartDate;
    private boolean status; //ACTIVE, INACTIVE
    private boolean expired = false;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLeaveBalanceId() {
        return leaveBalanceId;
    }

    public void setLeaveBalanceId(long leaveBalanceId) {
        this.leaveBalanceId = leaveBalanceId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public GroupPayroll getGroupPayroll() {
        return groupPayroll;
    }

    public void setGroupPayroll(GroupPayroll groupPayroll) {
        this.groupPayroll = groupPayroll;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public LeaveType getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(LeaveType leaveType) {
        this.leaveType = leaveType;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public Date getNextAddDate() {
        return nextAddDate;
    }

    public void setNextAddDate(Date nextAddDate) {
        this.nextAddDate = nextAddDate;
    }

    public Date getNextRestartDate() {
        return nextRestartDate;
    }

    public void setNextRestartDate(Date nextRestartDate) {
        this.nextRestartDate = nextRestartDate;
    }

    public Date getLastAddDate() {
        return lastAddDate;
    }

    public void setLastAddDate(Date lastAddDate) {
        this.lastAddDate = lastAddDate;
    }

    public Date getLastRestartDate() {
        return lastRestartDate;
    }

    public void setLastRestartDate(Date lastRestartDate) {
        this.lastRestartDate = lastRestartDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }
}
