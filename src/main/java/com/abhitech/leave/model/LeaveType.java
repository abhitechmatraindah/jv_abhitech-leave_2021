package com.abhitech.leave.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
public class LeaveType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Date createdDate = new Date();
    private boolean deductive;
    private int maxBalance;
    private int defaultBalance;
    //    private boolean once = false; // accomodated by addtype and restartType
    private String dayType; // WORKINGDATE, CALENDARDATE
    private boolean negativeAllowed = false;

    //LEAVE BALANCE EXPIRED
    private String expDateType; //DATE, DAYS, MONTHS, YEARS, NONE
    private String expDateValue;
    private String expStartDateType; // ENROLLEDDATE, LEAVEACTIVATIONDATE, PROBATIONENDDATE, CONTRACTDATE, SPECIFICDATE
    private String expStartDateTypeValue;
    //LEAVE BALANCE EXPIRED END

    //ADD TYPE
    private String addDateType; //DATE, DAYS, MONTHS, YEARS, NONE
    private String addDateValue;
    private int addAmount;
    private String addStartDateType; // ENROLLEDDATE, LEAVEACTIVATIONDATE, PROBATIONENDDATE, CONTRACTDATE, SPECIFICDATE
    private String addStartDateTypeValue;
    private boolean addActive = false;
    private boolean addRepeat = false;
    //END ADD TYPE

    //RESTART TYPE
    private String restartDateType; //DATE, DAYS, MONTHS, YEARS, NONE
    private String restartDateValue;
    private String restartStartDateType; // ENROLLEDDATE, LEAVEACTIVATIONDATE, PROBATIONENDDATE, CONTRACTDATE, SPECIFICDATE
    private String restartStartDateTypeValue;
    private boolean restartActive = false;
    private boolean restartRepeat = false;
    //END RESTART TYPE

    @Column(unique = true)
    private String leaveCode;
    private String leaveName;
    private int maxDuration = -1; // UNUSED
    private String gender;
    private String paidStatus; // PAID, UNPAID
    private int leaveValidity; // UNUSED
    private boolean isActive = false;
    private boolean expired = false;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date expiredDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isDeductive() {
        return deductive;
    }

    public void setDeductive(boolean deductive) {
        this.deductive = deductive;
    }

    public int getMaxBalance() {
        return maxBalance;
    }

    public void setMaxBalance(int maxBalance) {
        this.maxBalance = maxBalance;
    }

    public int getDefaultBalance() {
        return defaultBalance;
    }

    public void setDefaultBalance(int defaultBalance) {
        this.defaultBalance = defaultBalance;
    }

    public String getDayType() {
        return dayType;
    }

    public void setDayType(String dayType) {
        this.dayType = dayType;
    }

    public boolean isNegativeAllowed() {
        return negativeAllowed;
    }

    public void setNegativeAllowed(boolean negativeAllowed) {
        this.negativeAllowed = negativeAllowed;
    }

    public String getExpDateType() {
        return expDateType;
    }

    public void setExpDateType(String expDateType) {
        this.expDateType = expDateType;
    }

    public String getExpDateValue() {
        return expDateValue;
    }

    public void setExpDateValue(String expDateValue) {
        this.expDateValue = expDateValue;
    }

    public String getExpStartDateType() {
        return expStartDateType;
    }

    public void setExpStartDateType(String expStartDateType) {
        this.expStartDateType = expStartDateType;
    }

    public String getExpStartDateTypeValue() {
        return expStartDateTypeValue;
    }

    public void setExpStartDateTypeValue(String expStartDateTypeValue) {
        this.expStartDateTypeValue = expStartDateTypeValue;
    }

    public String getAddDateType() {
        return addDateType;
    }

    public void setAddDateType(String addDateType) {
        this.addDateType = addDateType;
    }

    public String getAddDateValue() {
        return addDateValue;
    }

    public void setAddDateValue(String addDateValue) {
        this.addDateValue = addDateValue;
    }

    public int getAddAmount() {
        return addAmount;
    }

    public void setAddAmount(int addAmount) {
        this.addAmount = addAmount;
    }

    public String getAddStartDateType() {
        return addStartDateType;
    }

    public void setAddStartDateType(String addStartDateType) {
        this.addStartDateType = addStartDateType;
    }

    public String getAddStartDateTypeValue() {
        return addStartDateTypeValue;
    }

    public void setAddStartDateTypeValue(String addStartDateTypeValue) {
        this.addStartDateTypeValue = addStartDateTypeValue;
    }

    public boolean isAddActive() {
        return addActive;
    }

    public void setAddActive(boolean addActive) {
        this.addActive = addActive;
    }

    public boolean isAddRepeat() {
        return addRepeat;
    }

    public void setAddRepeat(boolean addRepeat) {
        this.addRepeat = addRepeat;
    }

    public String getRestartDateType() {
        return restartDateType;
    }

    public void setRestartDateType(String restartDateType) {
        this.restartDateType = restartDateType;
    }

    public String getRestartDateValue() {
        return restartDateValue;
    }

    public void setRestartDateValue(String restartDateValue) {
        this.restartDateValue = restartDateValue;
    }

    public String getRestartStartDateType() {
        return restartStartDateType;
    }

    public void setRestartStartDateType(String restartStartDateType) {
        this.restartStartDateType = restartStartDateType;
    }

    public String getRestartStartDateTypeValue() {
        return restartStartDateTypeValue;
    }

    public void setRestartStartDateTypeValue(String restartStartDateTypeValue) {
        this.restartStartDateTypeValue = restartStartDateTypeValue;
    }

    public boolean isRestartActive() {
        return restartActive;
    }

    public void setRestartActive(boolean restartActive) {
        this.restartActive = restartActive;
    }

    public boolean isRestartRepeat() {
        return restartRepeat;
    }

    public void setRestartRepeat(boolean restartRepeat) {
        this.restartRepeat = restartRepeat;
    }

    public String getLeaveCode() {
        return leaveCode;
    }

    public void setLeaveCode(String leaveCode) {
        this.leaveCode = leaveCode;
    }

    public String getLeaveName() {
        return leaveName;
    }

    public void setLeaveName(String leaveName) {
        this.leaveName = leaveName;
    }

    public int getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPaidStatus() {
        return paidStatus;
    }

    public void setPaidStatus(String paidStatus) {
        this.paidStatus = paidStatus;
    }

    public int getLeaveValidity() {
        return leaveValidity;
    }

    public void setLeaveValidity(int leaveValidity) {
        this.leaveValidity = leaveValidity;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }
}
