$('#formCreateAdmin').submit(
    function (e) {
        e.preventDefault();
        $('#btnCreateAdminSubmit').prop('disabled', true);
        $.post("/createAdmin", $('#formCreateAdmin').serialize(),
            function (data) {
                $('#errorMessageCreateAdminContainer').show();
                $('#errorMessageCreateAdmin').html(
                    "Save status: " + data.message);
                $('#btnCreateAdminSubmit').prop('disabled', false);
            }, "json");
        this.reset();
    });

$( "[id*='btnCreateAdminSubmit']" ).click(function() {
    setTimeout(function(){
        window.open("/admin_create","_self");
    },1500);
});

$('#deleteModal').on('show.bs.modal', function (e) {
    $('#userId').val(e.relatedTarget.id);
})
$('#deleteModal').on('hidden.bs.modal', function () {
    location.reload();
})
$('#formDisableUser').submit(
    function (e) {
        e.preventDefault();
        $('#btnDisableUserSubmit').prop('disabled', true);

        $.post("/disableAdmin", $('#formDisableUser').serialize(),
            function (data) {
                $('#errorMessageDisableUserContainer').show();
                $('#errorMessageDisableUser').html(
                    "Save status: " + data.message);
                $('#btnDisableUserSubmit').prop('disabled', false);
            }, "json");
    });
