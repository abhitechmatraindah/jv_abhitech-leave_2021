$('#formUpdateLeaveType').submit(
    function (e) {
        e.preventDefault();
        $('#btnUpdateLeaveTypeSubmit').prop('disabled', true);
        $.post("/updateLeaveType", $('#formUpdateLeaveType').serialize(),
            function (data) {
                $('#errorMessageUpdateLeaveTypeContainer').show();
                $('#errorMessageUpdateLeaveType').html(
                    "Save status: " + data.message);
                $('#btnUpdateLeaveTypeSubmit').prop('disabled', false);
            }, "json");
    });
/*


/!*START FOR LEAVE TYPE TABLE UPDATE*!/
$('#formUpdateGroupPayrollLeaveTypes').submit(


function storeTblValues()
{
    var TableData = new Array();

    $('#leaveTypesTable tr').each(function(row, tr){
        TableData[row]={
            "record" : $(tr).find('td:eq(0)').text()
            , "leaveTypeIds" :$(tr).find('td:eq(1)').text()
            , "leaveName" : $(tr).find('td:eq(2)').text()
            , "defaultBalance" : $(tr).find('td:eq(3)').text()
        }
    });
    TableData.shift();  // first row will be empty - so remove

    $.ajax({
        type: "POST",
        url: "/updateGroupPayrollLeaveType" + TableData,
        success: function(msg){
            console.log(msg);
            // return value stored in msg variable
        }
    });
},);*/


/*$('#formCreateLeaveType').submit(
    function (e) {
        e.preventDefault();
        $('#btnCreateLeaveTypeSubmit').prop('disabled', true);
        $.post("/createLeaveType", $('#formCreateLeaveType').serialize(),
            function (data) {
                $('#errorMessageCreateLeaveTypeContainer').show();
                $('#errorMessageCreateLeaveType').html(
                    "Save status: " + data.message);
                $('#btnCreateLeaveTypeSubmit').prop('disabled', false);
            }, "json");
        this.reset();
    });

$('#formCreateLeaveType').submit(
    function (e) {
        e.preventDefault();
        $('#btnCreateLeaveTypeSubmit').prop('disabled', true);
        $.post("/createLeaveType", $('#formCreateLeaveType').serialize(),
            function (data) {
                $('#errorMessageCreateLeaveTypeContainer').show();
                $('#errorMessageCreateLeaveType').html(
                    "Save status: " + data.message);
                $('#btnCreateLeaveTypeSubmit').prop('disabled', false);
            }, "json");
        this.reset();
    });
*/
