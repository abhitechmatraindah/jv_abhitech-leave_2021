$('#editModalLeaveType').on('show.bs.modal', function (e) {
    var $modal = $(this);
    $.post("/getLeaveTypeById", {
        id: e.relatedTarget.id
    }, function (data, status) {
        $('#leaveId').val(data.object.id);
        $('#leaveName').val(data.object.leaveName);
        $('#leaveCode').val(data.object.leaveCode);
        $('#duration').val(data.object.duration);
        if (data.object.gender === "Male") {
            $('#male').prop('checked', true);
        } else {
            $('#female').prop('checked', true);
        }
        $('#paidStatus').val(data.object.paidStatus);
        $('#leaveValidity').val(data.object.leaveValidity);
        $('#maxBalance').val(data.object.maxBalance);
        $('#defaultBalance').val(data.object.defaultBalance);
        $('#dayType').val(data.object.dayType);
        $('#availableAfter').val(data.object.availableAfter);
        if (data.object.deductive) {
            $('#truedeductive').prop('checked', true);
        } else {
            $('#falsedeductive').prop('checked', true);
        }

        if (data.object.negativeAllowed) {
            $('#truenegative').prop('checked', true);
        } else {
            $('#falsenegative').prop('checked', true);
        }

        if (data.object.negativeAllowed) {
            $('#truenegative').prop('checked', true);
        } else {
            $('#falsenegative').prop('checked', true);
        }

        if (data.object.once) {
            $('#trueonce').prop('checked', true);
        } else {
            $('#falseonce').prop('checked', true);
        }

        if (data.object.status) {
            $('#truestatus').prop('checked', true);
        } else {
            $('#falsestatus').prop('checked', true);
        }
    }, "json");

})

$('#editModalLeaveType').on('hidden.bs.modal', function () {
    location.reload();
})


$('#editModalGroupPayroll').on('show.bs.modal', function (e) {
    var $modal = $(this);
    $.post("/getGroupPayrollById", {
        id: e.relatedTarget.id
    }, function (data, status) {
        $('#id').val(data.object.id);
        $('#groupCode').val(data.object.groupCode);
        $('#groupName').val(data.object.groupName);
        $('#project').val(data.object.project);
        if (data.object.defaultLeaveType.id != null) {
            $('#defaultLeaveType').val(data.object.defaultLeaveType.id);
        }
        else{
            $('#defaultLeaveType').val("Empty");
        }
    }, "json");
})

$('#editModalGroupPayroll').on('hidden.bs.modal', function () {
    location.reload();
})

$( "[id*='btnAdd']" ).click(function() {
    var v = $(this).val();
    $('#defaultLeaveType').val(v);
    $('#lookUpLeaveTypeModal').modal('toggle');
});

/*FOR EMPLOYEE PAGE*/
function viewUser(id) {
    var url = "/getEmployeeBalance/?id=" + id;
    $.ajax({
        url: url,
        success: function(response) {
            console.log(response);
            $('#viewModalHolder').html(response);
            $('#viewBalanceModal').modal('show');
        }
    });
}

function editBalance(id) {
    var url = "/getBalanceById/?id=" + id;
    $.ajax({
        url: url,
        success: function(response) {
            console.log(response);
            $('#editBalanceModalHolder').html(response);
            $('#editBalanceModal').modal('show');
        }
    });
}

function leaverequest(id) {
    var url = "/getEmployeeBalanceForRequestForm/?id=" + id;
    $.ajax({
        url: url,
        success: function(response) {
            console.log(response);
            $('#viewModalHolder').html(response);
        }
    });
}

$('#leaveRequestModal').on('show.bs.modal', function (e) {
    var $modal = $(this);
    $.post("/getEmployeeById", {
        id: e.relatedTarget.id
    }, function (data, status) {
        $('#fullName').val(data.object.employeeName);
        $('#employeeId').val(data.object.id);
    }, "json");

})

$('#leaveRequestModal').on('show.bs.modal', function (e) {
    var id = e.relatedTarget.id;
    var url = "/getLeaveTypeDropdownById/?id=" + id;
    $.ajax({
        url: url,
        success: function(response) {
            console.log(response);
            $('#leaveTypeDropdownHolder').html(response);
        }
    });
})

/*END FOR EMPLOYEE PAGE*/
/*FOR GROUP PAYROLL PAGE*/
function getleavetypes(id) {
    var url = "/getLeaveTypesTable/?id=" + id;
    $.ajax({
        url: url,
        success: function(response) {
            console.log(response);
            $('#tableHolder').html(response);
        }
    });
}
/*FOR ADDING LEAVE TYPES*/

$( "[id*='btnLeaveTypeAdd']" ).click(function() {
    var input = $("#leaveTypeIds");
    var id = this.getAttribute('data1');
    var leavename = this.getAttribute('data2');
    var balance = this.getAttribute('data3');
    var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + id + "</td><td>" + leavename + "</td><td>" + balance + "</td></tr>";
    $('#leaveTypesTableBody').append(markup);
    input.val(input.val() + id + ", ");
    $(this).parents("tr").hide();
});


/*FOR COmPARING TABLE CONTENTS AND ADDING VALUE TO HIDDEN INPUT*/
/*CHECK THIS SCRIPT FUNCTIONING ON MODAL_CONTENT.HTML*/
/*$('#editModalGroupPayroll').on('show.bs.modal', function (e){
    setTimeout(function(){
        $('#leaveTypesTable > tbody >tr').each(function(){

            var currentRowHTML=$(this).find(".leaveTypeClass").html();
            var input = $("#leaveTypeIds");
            input.val(input.val() + currentRowHTML + ", ");

            $('#leaveTypesAddTable > tbody > tr').each(function(){
                if($(this).find(".leaveTypeClass").html()===currentRowHTML){
                    $(this).remove();
                }
            });
        });
    }, 1500);
})*/
