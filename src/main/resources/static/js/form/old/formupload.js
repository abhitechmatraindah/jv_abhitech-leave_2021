$('#formCreateLeaveType').submit(
    function (e) {
        e.preventDefault();
        $('#btnCreateLeaveTypeSubmit').prop('disabled', true);
        $.post("/createLeaveType", $('#formCreateLeaveType').serialize(),
            function (data) {
                $('#errorMessageCreateLeaveTypeContainer').show();
                $('#errorMessageCreateLeaveType').html(
                    "Save status: " + data.message);
                $('#btnCreateLeaveTypeSubmit').prop('disabled', false);
            }, "json");
    });

$('#leaveRequestForm').submit(
    function (e) {
        e.preventDefault();
        $('#btnCreateLeaveRequestSubmit').prop('disabled', true);
        $.post("/createLeaveRequest", $('#leaveRequestForm').serialize(),
            function (data) {
                $('#errorMessageCreateLeaveRequestContainer').show();
                $('#errorMessageCreateLeaveRequest').html(
                    "Save status: " + data.message);
                $('#btnCreateLeaveRequestSubmit').prop('disabled', false);
            }, "json");
    });

/*$('#formCreateLeaveType').submit(
    function (e) {
        e.preventDefault();
        $('#btnCreateLeaveTypeSubmit').prop('disabled', true);
        $.post("/createLeaveType", $('#formCreateLeaveType').serialize(),
            function (data) {
                $('#errorMessageCreateLeaveTypeContainer').show();
                $('#errorMessageCreateLeaveType').html(
                    "Save status: " + data.message);
                $('#btnCreateLeaveTypeSubmit').prop('disabled', false);
            }, "json");
        this.reset();
    });

$('#formCreateLeaveType').submit(
    function (e) {
        e.preventDefault();
        $('#btnCreateLeaveTypeSubmit').prop('disabled', true);
        $.post("/createLeaveType", $('#formCreateLeaveType').serialize(),
            function (data) {
                $('#errorMessageCreateLeaveTypeContainer').show();
                $('#errorMessageCreateLeaveType').html(
                    "Save status: " + data.message);
                $('#btnCreateLeaveTypeSubmit').prop('disabled', false);
            }, "json");
        this.reset();
    });
*/
