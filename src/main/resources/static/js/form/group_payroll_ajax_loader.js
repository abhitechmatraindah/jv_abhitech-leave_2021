function getTables(id) {
    var url1 = "/getLeaveTypesTable/?id=" + id;
    var url2 = "/getPastLeaveTypesTable/?id=" + id;
    var url3 = "/getBindableLeaveTypes/?id=" + id;
    $.ajax({
        url: url1,
        success: function (response) {
            console.log(response);
            $('#tableHolder').html(response);
        }
    });
    $.ajax({
        url: url2,
        success: function (response) {
            console.log(response);
            $('#tableHolderPastLeaveTypes').html(response);
        }
    });
    $.ajax({
        url: url3,
        success: function (response) {
            console.log(response);
            $('#bindableLeaveTypesHolder').html(response);
        }
    });

}

$('#bindModalGroupPayroll').on('show.bs.modal', function (e) {
    var $modal = $(this);
    $.post("/getGroupPayrollById", {
        id: e.relatedTarget.id
    }, function (data, status) {
        $('#groupId').val(data.object.id);
        $('#groupCode').val(data.object.groupCode);
        $('#groupName').val(data.object.groupName);
        $('#project').val(data.object.project.projectName);
        if (data.object.defaultLeaveType.id != null) {
            $('#defaultLeaveType').val(data.object.defaultLeaveType.leaveName);
        } else {
            $('#defaultLeaveType').val("Empty");
        }
    }, "json");
})

$('#bindModalGroupPayroll').on('hidden.bs.modal', function () {
    location.reload();
})

$("[id*='addButton']").click(function () {
    var v = $(this).val();
    var leaveName = this.getAttribute('data1');
    $('#defaultLeaveTypeIdX').val(v);
    $('#defaultLeaveTypeX').val(leaveName);
    $('#lookUpLeaveTypeModal').modal('toggle');
});

$('#addDefaultLeaveType').on('show.bs.modal', function (e) {
    $('#defaultLeaveTypeX').val("");
    $('#defaultLeaveTypeIdX').val("");
    $.post("/getGroupPayrollById", {
        id: e.relatedTarget.id
    }, function (data, status) {
        $('#groupIdX').val(data.object.id);
        $('#groupCodeX').val(data.object.groupCode);
        $('#groupNameX').val(data.object.groupName);
        $('#projectX').val(data.object.project.projectName);
        if (data.object.defaultLeaveType != null) {
            $('#defaultLeaveTypeX').val(data.object.defaultLeaveType.leaveName);
            $('#defaultLeaveTypeIdX').val(data.object.defaultLeaveType.id);
        } else {
            $('#defaultLeaveTypeX').val("Empty");
        }
    }, "json");
})

$('#addDefaultLeaveType').on('hidden.bs.modal', function () {
    location.reload();
})

$(document).ready(function () {
    // Find and remove selected table rows
    $(".delete-row").click(function () {
        $("table tbody").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {
                $(this).parents("tr").remove();
                $('#leaveTypesAddTable > tbody > tr').show();
            }
        });
    });
})

$(window).on("load", function() {
    $("#toggle-show-inactive").trigger('click');
});
$(function() {
    $('#toggle-show-inactive').change(function() {
        if($(this).prop('checked')){
            $("#maintable td:nth-of-type(5):contains('In-active')").parent().hide();
            $("#maintable td:nth-of-type(6):contains('(Expired)')").parent().hide();
        } else {
            $("#maintable td:nth-of-type(5):contains('In-active')").parent().show();
            $("#maintable td:nth-of-type(6):contains('(Expired)')").parent().show();
        }
    })
});