
$('#formCreateLeaveRequest').submit(
    function (e) {
        e.preventDefault();
        $('#btnCreateRequest').prop('disabled', true);
        $.post("/createLeaveRequest", $('#formCreateLeaveRequest').serialize(),
            function (data) {
                $('#errorMessageLeaveRequestContainer').show();
                $('#errorMessageLeaveRequest').html(
                    "Save status: " + data.message);
                $('#btnCreateRequest').prop('disabled', false);
            }, "json");
    });

$('#duration').change(function () {
    var start = jQuery("#dateFrom").datepicker({dateFormat : "yy-mm-dd"}).datepicker("getDate");
    var duration = $('#duration').val()-1;
    start.setDate(start.getDate() + parseInt(duration, 10));
    jQuery("#dateTo").datepicker({dateFormat : "yy-mm-dd"}).datepicker("setDate", start)
});