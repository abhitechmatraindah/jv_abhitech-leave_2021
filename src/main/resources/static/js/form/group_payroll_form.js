$('#formAddDefaultLeaveType').submit(
    function (e) {
        e.preventDefault();
        $('#btnAddDefaultLeaveTypeSubmit').prop('disabled', true);
        $.post("/updateGroupPayroll", $('#formAddDefaultLeaveType').serialize(),
            function (data) {
                $('#errorMessageDefaultLeaveTypeContainer').show();
                $('#errorMessageDefaultLeaveType').html(
                    "Save status: " + data.message);
                $('#btnAddDefaultLeaveTypeSubmit').prop('disabled', false);
            }, "json");
    });

$('#formUpdateGroupPayroll').submit(
    function (e) {
        e.preventDefault();
        var col_Array1 = $('#leaveTypesTableBody td:nth-child(2)').map(function(){
            return $(this).text();
        }).get();
        var col_Array2 = $('#PastLeaveTypesTableBody td:nth-child(1)').map(function(){
            return $(this).text();
        }).get();
        var col_Array_Final = col_Array1.concat(col_Array2);
        $("#leaveTypeIds").val(col_Array_Final.join(", "));

        $('#btnUpdateGroupPayrollSubmit').prop('disabled', true);
        var ids = $('#leaveTypeIds').val();
        var leaveTypeIds = ids.split(', ');
        leaveTypeIds = leaveTypeIds.join();
        var id = $("#groupId").val();
        /*$('#leaveTypeIds').val(data);*/

        $.post("/updateGroupPayrollLeaveType", {"id": id, "leaveTypeIds": leaveTypeIds},
            function (data) {
                    $('#errorMessageGroupPayrollContainer').show();
                    $('#errorMessageGroupPayroll').html(
                        "Save status: " + data.message);
                    $('#btnUpdateGroupPayrollSubmit').prop('disabled', false);
            }, "json");
    });;
