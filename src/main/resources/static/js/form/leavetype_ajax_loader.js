$( "[id*='btnUpdateLeaveTypeSubmit']" ).click(function() {
    setTimeout(function(){
        window.open("/leave_type_manage","_self");
    },1000);
});

$( "[id*='btnCancelUpdateLeaveType']" ).click(function() {
    setTimeout(function(){
        window.open("/leave_type_manage","_self");
    },500);
});

/*$('#editModalLeaveType').on('show.bs.modal', function (e) {
    $(this).find('.modal-body').css({
        'max-height':'100%'
    });
    var $modal = $(this);
    $.post("/getLeaveTypeById", {
        id: e.relatedTarget.id
    }, function (data, status) {
        $('#leaveId').val(data.object.id);
        $('#leaveName').val(data.object.leaveName);
        $('#leaveCode').val(data.object.leaveCode);
        $('#duration').val(data.object.maxDuration);
        $('#gender').val(data.object.gender);
        $('#paidStatus').val(data.object.paidStatus);
        var d = new Date(data.object.expiredDate),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        var a = [year, month, day].join('-');
        $('#expiredDate').val(a);
        /!*ACTIVE*!/
        if (data.object.active === true){
            $('#active').val("1");
        } else if (data.object.active === false){
            $('#active').val("0");
        }
        /!*EXPIRED*!/
        if (data.object.expired === true){
            $('#expired').val("1");
        } else if (data.object.expired === false){
            $('#expired').val("0");
        }
        $('#maxBalance').val(data.object.maxBalance);
        $('#defaultBalance').val(data.object.defaultBalance);
        /!*deductive*!/
        if (data.object.deductive === true){
            $('#deductive').val("1");
        } else if (data.object.deductive === false){
            $('#deductive').val("0");
        }
        /!*negativeAllowed*!/
        if (data.object.negativeAllowed === true){
            $('#negativeAllowed').val("1");
        } else if (data.object.negativeAllowed === false){
            $('#negativeAllowed').val("0");
        }
        $('#dayType').val(data.object.dayType);

        $('#expDateType').val(data.object.expDateType);
        $('#expDateValueText').val(data.object.expDateValue);
        $('#expDateValueDate').val(data.object.expDateValue);
        $('#expStartDateType').val(data.object.expStartDateType);
        $('#expStartDateTypeValue').val(data.object.expStartDateTypeValue);


        $('#addDateType').val(data.object.addDateType);
        $('#addDateValueDate').val(data.object.addDateValue);
        $('#addDateValueText').val(data.object.addDateValue);
        $('#addAmount').val(data.object.addAmount);
        $('#addStartDateType').val(data.object.addStartDateType);
        $('#addStartDateTypeValue').val(data.object.addStartDateTypeValue);
        /!*negativeAllowed*!/
        if (data.object.addActive === true){
            $('#addActive').val("1");
        } else if (data.object.addActive === false){
            $('#addActive').val("0");
        }
        /!*negativeAllowed*!/
        if (data.object.addRepeat === true){
            $('#addRepeat').val("1");
        } else if (data.object.addRepeat === false){
            $('#addRepeat').val("0");
        }

        $('#restartDateType').val(data.object.restartDateType);
        $('#restartDateValueDate').val(data.object.restartDateValue);
        $('#restartDateValueText').val(data.object.restartDateValue);
        $('#restartStartDateType').val(data.object.restartStartDateType);
        $('#restartStartDateTypeValue').val(data.object.restartStartDateTypeValue);
        /!*negativeAllowed*!/
        if (data.object.restartActive === true){
            $('#restartActive').val("1");
        } else if (data.object.restartActive === false){
            $('#restartActive').val("0");
        }
        /!*negativeAllowed*!/
        if (data.object.restartRepeat === true){
            $('#restartRepeat').val("1");
        } else if (data.object.restartRepeat === false){
            $('#restartRepeat').val("0");
        }
    }, "json");
})*/
