/*FOR EMPLOYEE PAGE*/
function viewUser(id) {
    var urlBalance = "/getEmployeeBalance/?id=" + id
    var urlEmployee = "/getEmployeeById/?id=" + id
    $.ajax({
        url: urlBalance,
        success: function(response) {
            console.log(response);
            $('#viewModalHolder').html(response);
            $('#viewEmployeeModal').modal('show');
        }
    });
    $('#viewEmployeeModal').on('show.bs.modal', function (e) {
        $.post("/getEmployeeById", {
            id: id
        }, function (data, status) {
            $('#employeeId').val(data.object.employeeId);
            $('#employeeName').val(data.object.employeeName);
            $('#gender').val(data.object.gender);
            $('#groupName').val(data.object.groupPayroll.groupName);
            $('#projectName').val(data.object.groupPayroll.project.projectName);
            $('#clientName').val(data.object.groupPayroll.project.client.clientName);

            $('#dateEnrolled').val(formatDate(data.object.dateEnrolled));
            $('#probationEndDate').val(formatDate(data.object.probationEndDate));
            $('#contractDate').val(formatDate(data.object.contractStartDate) + ' - ' + formatDate(data.object.contractEndDate));
            $('#leaveActivationDate').val(formatDate(data.object.leaveActivationDate));

            function formatDate(dateString){
                if(dateString != null){
                    bound = dateString.indexOf('T');
                    var dateData = dateString.slice(0, bound).split('-');
                    return dateData.join('/');
                }
                return 'No Data';
            }

        }, "json");

    })
}
/*
function editBalance(id) {
    var url = "/getBalanceById/?id=" + id;
    $.ajax({
        url: url,
        success: function(response) {
            console.log(response);
            $('#editBalanceModalHolder').html(response);
            $('#editBalanceModal').modal('show');
        }
    });
}*/

$('#viewEmployeeModal').on('hidden.bs.modal', function () {
    location.reload();
})

$('#editBalanceModal').on('show.bs.modal', function (e) {
    $('#leaveBalanceId').val("");
    $('#adjustBalance').val("");
    $('#currentBalance').val("");
    var currBal = e.relatedTarget.getAttribute('data1');
    $('#currentBalance').val(currBal);
    $('#leaveBalanceId').val(e.relatedTarget.id);
})

$( "[id='btnAddBalance']" ).click(function (e) {
    e.preventDefault();
    $.post("/leaveBalance/addBalance", $('#formUpdateEmployeeBalance').serialize(),
        function (data) {
            $('#errorMessageLeaveBalanceContainer').show();
            $('#errorMessageLeaveBalance').html(
                "Save status: " + data.message);
        }, "json");
    $( "[id='clsBttnEdit']" ).click(function () {
        location.reload();
    });
});

$( "[id='btnDeductBalance']" ).click(function (e) {
    e.preventDefault();
    $.post("/leaveBalance/deductBalance", $('#formUpdateEmployeeBalance').serialize(),
        function (data) {
            $('#errorMessageLeaveBalanceContainer').show();
            $('#errorMessageLeaveBalance').html(
                "Save status: " + data.message);
        }, "json");
    $( "[id='clsBttnEdit']" ).click(function () {
        location.reload();
    });
});

$(window).on("load", function() {
    $("#toggle-show-inactive").trigger('click');
});
$(function() {
    $('#toggle-show-inactive').change(function() {
        if($(this).prop('checked')){
            $("#maintable td:nth-of-type(6):contains('Expired')").parent().hide();
        } else {
            $("#maintable td:nth-of-type(6):contains('Expired')").parent().show();
        }
    })
});