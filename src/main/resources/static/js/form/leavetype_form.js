$('#formCreateLeaveType').submit(
    function (e) {
        e.preventDefault();
        $('#btnCreateLeaveTypeSubmit').prop('disabled', true);
        $.post("/createLeaveType", $('#formCreateLeaveType').serialize(),
            function (data) {
                $('#errorMessageCreateLeaveTypeContainer').show();
                $('#errorMessageCreateLeaveType').html(
                    "Save status: " + data.message);
                $('#btnCreateLeaveTypeSubmit').prop('disabled', false);
            }, "json");;
    });

$('#formUpdateLeaveType').submit(
    function (e) {
        e.preventDefault();
        $('#btnUpdateLeaveTypeSubmit').prop('disabled', true);
        $.post("/updateLeaveType", $('#formUpdateLeaveType').serialize(),
            function (data) {
                $('#errorMessageUpdateLeaveTypeContainer').show();
                $('#errorMessageUpdateLeaveType').html(
                    "Save status: " + data.message);
                $('#btnUpdateLeaveTypeSubmit').prop('disabled', false);
            }, "json");
    });