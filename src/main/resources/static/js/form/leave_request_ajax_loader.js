/*FOR LEAVE REQUEST PAGE*/
$(window).on("load", function() {
    $("#toggle-show-inactive").trigger('click');
});
$(function() {
    $('#toggle-show-inactive').change(function() {
        if($(this).prop('checked')){
            $("#maintable td:nth-of-type(6):contains('Expired')").parent().hide();
        } else {
            $("#maintable td:nth-of-type(6):contains('Expired')").parent().show();
        }
    })
});
function viewUser(id) {
    var url = "/getEmployeeBalanceForRequestForm/?id=" + id
    $.ajax({
        url: url,
        success: function(response) {
            console.log(response);
            $('#viewModalHolder').html(response);
            $('#viewEmployeeModal').modal('show');
        }
    });
    $('#viewEmployeeModal').on('show.bs.modal', function (e) {
        $.post("/getEmployeeById", {
            id: id
        }, function (data, status) {
            $('#employeeId').val(data.object.employeeId);
            $('#employeeName').val(data.object.employeeName);
            $('#gender').val(data.object.gender);
            $('#groupName').val(data.object.groupPayroll.groupName);
            $('#projectName').val(data.object.groupPayroll.project.projectName);
            $('#clientName').val(data.object.groupPayroll.project.client.clientName);

            $('#dateEnrolled').val(formatDate(data.object.dateEnrolled));
            $('#probationEndDate').val(formatDate(data.object.probationEndDate));
            $('#contractDate').val(formatDate(data.object.contractStartDate) + ' - ' + formatDate(data.object.contractEndDate));
            $('#leaveActivationDate').val(formatDate(data.object.leaveActivationDate));
            function formatDate(dateString){
                if(dateString != null){
                    bound = dateString.indexOf('T');
                    var dateData = dateString.slice(0, bound).split('-');
                    return dateData.join('/');
                }
                return 'No Data';
            }
        }, "json");

    })
}

$('#createLeaveRequestModal').on('show.bs.modal', function (e) {
    $('#leaveId').val(e.relatedTarget.id)
    var leaveName = e.relatedTarget.getAttribute("data1");
    $('#leaveName').val(leaveName);
})

$('#detailModal').on('show.bs.modal', function (e) {
    var $modal = $(this);
    $.post("/getLeaveRequestById", {
        id: e.relatedTarget.id
    }, function (data, status) {
        $('#id').val(data.object.id);
        $('#createdDate').val(data.object.createdDate);
        $('#leaveBalance').val(data.object.leaveBalance.balance);
        $('#employeeName').val(data.object.leaveBalance.employee.employeeName);
        $('#amount').val(data.object.amount);
        $('#dateFrom').val(data.object.dateFrom);
        $('#dateTo').val(data.object.dateTo);
        $('#reason').val(data.object.reason);
        $('#remarks').val(data.object.remarks);
        $('#status').val(data.object.status);
        $('#balanceBefore').val(data.object.balanceBefore);
        $('#balanceAfter').val(data.object.balanceAfter);
    }, "json");
})


$('#deleteModal').on('show.bs.modal', function (e) {
    $('#leaveRequestId').val(e.relatedTarget.id);
})

$('#detailModal').on('hidden.bs.modal', function () {
    location.reload();
})

$('#deleteModal').on('hidden.bs.modal', function () {
    location.reload();
})

$('#formDeleteLeaveRequest').submit(
    function (e) {
        e.preventDefault();
        $('#btnDeleteLeaveTypeSubmit').prop('disabled', true);

        $.post("/cancelLeaveRequest", $('#formDeleteLeaveRequest').serialize(),
            function (data) {
                $('#errorMessageDeleteLeaveRequestContainer').show();
                $('#errorMessageDeleteLeaveRequest').html(
                    "Save status: " + data.message);
                $('#btnDeleteLeaveTypeSubmit').prop('disabled', false);
            }, "json");
    });

$(window).on("load", function() {
    $("#toggle-show-inactive").trigger('click');
});
$(function() {
    $('#toggle-show-inactive').change(function() {
        if($(this).prop('checked')){
            $("#maintable td:nth-of-type(8):contains('CANCELLED')").parent().hide();
            $("#maintable td:nth-of-type(8):contains('ENDED')").parent().hide();
        } else {
            $("#maintable td:nth-of-type(8):contains('CANCELLED')").parent().show();
            $("#maintable td:nth-of-type(8):contains('ENDED')").parent().show();
        }
    })
});