$(window).on("load", function() {
    $("#toggle-show-inactive").trigger('click');
});
$(function() {
    $('#toggle-show-inactive').change(function() {
        if($(this).prop('checked')){
            $("#maintable td:nth-of-type(18):contains('INACTIVE')").parent().hide();
        } else {
            $("#maintable td:nth-of-type(18):contains('INACTIVE')").parent().show();
        }
    })
});