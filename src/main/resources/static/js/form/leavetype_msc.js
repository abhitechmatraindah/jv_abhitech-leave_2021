$('#expDateType').change(function () {
    var Date = $('#expDateValueDate');
    var Text = $('#expDateValueText');

    var bEnabled = $(this).val() === "DATE";
    if (bEnabled) {
        Text.prop("disabled", true);
        Date.prop("disabled", false);
        Text.hide();
        Date.show();
    } else if ($(this).val() === "NONE") {
        Date.prop("disabled", true);
        Text.prop("disabled", true)
        Date.hide();
        Text.show();
    } else {
        Date.prop("disabled", true);
        Text.prop("disabled", false)
        Date.hide();
        Text.show();
    }
}).change(); // this .change triggers the code to run when the page is ready.

$('#expStartDateType').change(function () {
    var val = $('#expStartDateTypeValue');

    var bEnabled = $(this).val() === "SPECIFICDATE";
    if (bEnabled) {
        val.prop("disabled", false);
    } else {
        val.prop("disabled", true);
    }

}).change(); // this .change triggers the code to run when the page is ready.

$('#addDateType').change(function () {
    var Date = $('#addDateValueDate');
    var Text = $('#addDateValueText');

    var bEnabled = $(this).val() === "DATE";
    if (bEnabled) {
        Text.prop("disabled", true);
        Date.prop("disabled", false);
        Text.hide();
        Date.show();
    } else if ($(this).val() === "NONE") {
        Date.prop("disabled", true);
        Text.prop("disabled", true)
        Date.hide();
        Text.show();
    } else {
        Date.prop("disabled", true);
        Text.prop("disabled", false)
        Date.hide();
        Text.show();
    }
}).change(); // this .change triggers the code to run when the page is ready.

$('#addStartDateType').change(function () {
    var val = $('#addStartDateTypeValue');
    var bEnabled = $(this).val() === "SPECIFICDATE";
    if (bEnabled) {
        val.prop("disabled", false);
    } else {
        val.prop("disabled", true);
    }
}).change(); // this .change triggers the code to run when the page is ready.

$('#restartDateType').change(function () {
    var Date = $('#restartDateValueDate');
    var Text = $('#restartDateValueText');

    var bEnabled = $(this).val() === "DATE";
    if (bEnabled) {
        Text.prop("disabled", true);
        Date.prop("disabled", false);
        Text.hide();
        Date.show();
    } else if ($(this).val() === "NONE") {
        Date.prop("disabled", true);
        Text.prop("disabled", true)
        Date.hide();
        Text.show();
    } else {
        Date.prop("disabled", true);
        Text.prop("disabled", false)
        Date.hide();
        Text.show();
    }
}).change(); // this .change triggers the code to run when the page is ready.

$('#restartStartDateType').change(function () {
    var val = $('#restartStartDateTypeValue');

    var bEnabled = $(this).val() === "SPECIFICDATE";
    if (bEnabled) {
        val.prop("disabled", false);
    } else {
        val.prop("disabled", true);
    }
}).change(); // this .change triggers the code to run when the page is ready.