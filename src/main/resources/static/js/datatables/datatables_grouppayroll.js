$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#maintable thead tr').clone(true).appendTo( '#maintable thead' );
    $('#maintable thead tr:eq(1) th').each( function (i) {
        if (i > 2) {
            return false;
        }
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
    var table = $('#maintable').DataTable( {
        "columnDefs": [
            { "searchable": false, "targets": 3}
        ],
        orderCellsTop: true,
        fixedHeader: true,
        autoWidth: true,
        paging: false

    } );
} );

$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#maintablebinding thead tr').clone(true).appendTo( '#maintablebinding thead' );
    $('#maintablebinding thead tr:eq(1) th').each( function (i) {
        if (i > 3) {
            return false;
        }
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    var table = $('#maintablebinding').DataTable( {
        "columnDefs": [
            { "searchable": false, "targets": 4}
        ],
        orderCellsTop: true,
        fixedHeader: true,
        autoWidth: true,
        paging: false
    } );
} );